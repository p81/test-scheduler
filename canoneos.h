#ifndef CAMCONTROLLER_H
#define CAMCONTROLLER_H

#include <QStringList>
#include <QObject>
#include <QSharedPointer>
#include <QEventLoop>
#include <QTimer>
#include <EDSDK.h>

class CanonEOSUpdater : public QObject
{
    Q_OBJECT
  public slots:
    void update();
};

class CanonEOSThread : public QThread
{
    Q_OBJECT
  public:
    static CanonEOSThread * instance();
    static bool deleteInstance();
  protected:
    CanonEOSThread();
    virtual ~CanonEOSThread();
    // execute thread
    virtual void run();

    static CanonEOSThread *  self;
    static int               ref_count;
    CanonEOSUpdater          updater;
    QTimer                   update_timer;
};

class CanonEOS : public QObject
{
    Q_OBJECT
    friend class CanonEOSUpdater;

    typedef struct CanonCamera
    {
      CanonCamera()
      {
        ref = 0;
        is_register = false;
        is_open = false;
      };
      EdsCameraRef      ref;
      QString           name;
      QString           id;
      bool              is_register;
      bool              is_open;
    };

  public:
    CanonEOS();
    ~CanonEOS();

    static bool fillCameraList(QMap<QString, QSharedPointer<CanonCamera> > & cam_map_loc, QMap<QString, QString> & names);
    static void retriveCamList();

    static QMap<QString, QString> getCamNames();

  public slots:
    void registerCamera(const QString & _id);
    void unregisterCamera();
    void takePhoto();

  private:
    static EdsError EDSCALLBACK handleObjectEvent(EdsObjectEvent event,
                                                  EdsBaseRef     object,
                                                  EdsVoid *      context);

    static EdsError EDSCALLBACK handlePropertyEvent(EdsPropertyEvent  event,
                                                    EdsPropertyID     property,
                                                    EdsUInt32         param,
                                                    EdsVoid *         context);

    static EdsError EDSCALLBACK handleStateEvent(EdsStateEvent event,
                                                 EdsUInt32     parameter,
                                                 EdsVoid *     context);

    void downloadImage(EdsDirectoryItemRef directoryItem);
    void openSession(bool open);
  protected:
    CanonEOSThread &  canon_thread;

    uchar            *pixels;
    int               max_pixels_size;
    int               pixels_size;
    QSharedPointer<CanonCamera>  camera;

    static QReadWriteLock    map_lock;
    static QMap<QString, QString> cam_names;
    static QMap<QString, QSharedPointer<CanonCamera> > cam_map;
    static QMap<QString, QSharedPointer<CanonCamera> > reconnect_map;
    static QMap<QString, CanonEOS *> queue;
  signals:
    void downloadCompleted(const uchar * data, int size);
};

#endif // CAMCONTROLLER_H
