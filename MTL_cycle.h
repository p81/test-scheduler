#ifndef MTL_CYCLE_H
#define MTL_CYCLE_H

#include "MTLtask.h"
#include <QDoubleSpinBox>

class MtlCycleTask : public MtlTask
{
    Q_OBJECT

    int   uCtrlCh; // Control channel (actuator No) on this station (starting from zero)
    int   iMode;   // Control Mode (0-Stroke, 1-Load or 2-Straiin) to be imposed
    float fMean;   // Mean in engineering units
    float fAmplitude; // Amplitude in engineering units
    float fFrequency; // Cycling frequency in Hz (or rate/s if bConstantRate = 1 (TRUE))
    unsigned uQCycles; // No of quarter cycles to be generated. 4 will give one complete cycle
    unsigned uQuadrant; // Start point (0-mean down, 1- min, 2 - mean-up, 3-max)
    unsigned uWaveform; // 0-sine, 1-Ramp, 2-Pulse
    int bRelative; // if =0, specified mean is absolute. If =1, specified mean is shift from existing readout
    int bAdaptiveControl; // =1 if TRUE. Then, adaptive control will be imposed
    int bConstantRate; // if rate is to be imposed rather than frequency
    int bIncCycleCount; // =1 - cycle counter will be updated. =0 cyclecount will remain unchanged
    int bIncBlockCount; // = 1 to increment block count upon this call. =0 to leave count unchanged
    unsigned uEndQuadrant; // End point (0-mean down, 1- min, 2 - mean-up, 3-max)

    bool bMinMax;

    QDoubleSpinBox * qdsbMean;
    QDoubleSpinBox * qdsbAmplitude;
    QDoubleSpinBox * qdsbMin;
    QDoubleSpinBox * qdsbMax;

  public:
    MtlCycleTask();
    virtual void createForm(QFormLayout *layout);
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);
  protected:
    virtual void run();
  protected slots:
    void setuCtrlCh(int val);
    void setiMode(int val);
    void setfMean(double val);
    void setfAmplitude(double val);
    void setfFrequency(double val);
    void setuQCycles(int val);
    void setuQuadrant(int val);
    void setuEndQuadrant(int val);
    void setuWaveform(int val);
    void setbRelative(int val);
    void setbAdaptiveControl(int val);
    void setbConstantRate(int val);
    void setbCycleCount(int val);
    void setbIncBlockCount(int val);
    void setMin(double val);
    void setMax(double val);
    void setbMinMax(bool val);
};

#endif // MTL_CYCLE_H
