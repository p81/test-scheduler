#ifndef BASLER_H
#define BASLER_H

#include <QComboBox>
#include <QLineEdit>
#include <QTimer>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QMutex>
#include <QWaitCondition>
#include "task.h"
#include "linefilebrowse.h"
#include "baslercap.h"

using namespace Pylon;
using namespace Basler_GigECameraParams;


class ReconnectThread : public QThread
{
  protected:
    virtual void run();
};

class BaslerTask : public Task, public CImageEventHandler
{
    Q_OBJECT

    BaslerCamera basler_camera;
    friend class ReconnectThread;

    static QStringList FileFormatExt;
    static ReconnectThread            reconnect_thread;
    QString cam_name;

    int     has_settings;  // 1 - load settings | 0 - doesn't load
    QString settings_file; // settings filename
    LineFileBrowse * settings_file_le;
    QString out_dir;       // output directory
    LineFileBrowse * outdir_le;
    QString filename_template; // template for output filename
    int     multiple_shot;  // 1 - multiple shoot mode | 0 - single shoot mode
    int     timeout; // timeout between shoots in multiple mode
    bool    setfps;
    double  fps;
    int     file_format;

    QDoubleSpinBox * fps_sb;
    QCheckBox * fps_chb;
    QSpinBox * timeout_sb;
    QCheckBox * timeout_chb;
    QComboBox * cam_cb;
    QComboBox * vars_cb;
    QLineEdit * filename_template_le;

    QVariantList var_list;
    QMap<QString,QVariant>  vars;

    bool isTestShoot;
    QMutex frame_mutex;
    QWaitCondition grab_result;
    int multiple_shoot_count;

    QTimer timer;
  public:
    BaslerTask();
    ~BaslerTask();
    virtual void createForm(QFormLayout *layout);
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);
    virtual void initialize();
    virtual void finalize();

    virtual void OnImageGrabbed(CInstantCamera& _camera, const CGrabResultPtr& ptrGrabResult);
  protected:
    virtual void run();
  protected slots:
    void setCamIndex(const QString &val);
    void setVarCamName(const QString & val);
    void setCamSettings(int val);
    void setSettingsFile();
    void setOutDir();
    void setFilenameTemplate(const QString &val);
    void setMultipleShot(int val);
    void setTimeout(int val);
    void addVarToTemplate();
    void testShoot();
    void setFileFormat(int val);
    void setFrameCount(int val);
    void setFPS(double val);
    void toggleFPS(bool val);
    void exceptionMessage(const QString & msg);
  protected:
    void loadSettings();
    void test();

  signals:
    void beginThread();
    void endThread();
};

#endif // BASLER_H
