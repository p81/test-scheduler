#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QReadWriteLock>
#include "task.h"

namespace Ui {
class MainWindow;
}

class RunThread : public QThread
{
    Q_OBJECT
  protected:
    virtual void run();
  signals:
    void setBlockNumber(int li);
    void setBlockBkg(int bi, const QBrush & block_bkg);
    int showMsg();
};

class MainWindow : public QMainWindow
{
  Q_OBJECT

  public:
    static MainWindow * main_window;

  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void createActions();
    void createMenu();
    int execEndOfStep(int eostype);

  public slots:
    void updateTable();
    void createTable();

    void run();
    void breakLoop(bool b);
    void on_ignorepowerstatus_chb_toggled(bool b);
    void setPowerOn(bool b);
  private slots:
    void open();

    void on_addtask_btn_clicked();
    void on_addblock_btn_clicked();
    void on_rem_btn_clicked();

    void on_task_tree_clicked(const QModelIndex &index);

    void on_task_tree_activated(const QModelIndex &index);

    void on_start_stop_btn_clicked();
    void end_of_step_cb_currentIndexChange(int index);

    void on_saveas_btn_clicked();
    void on_load_btn_clicked();

    void on_connect_btn_clicked();

    void on_values_tbl_cellChanged(int row, int column);

    void on_BlockBkg_changed(int bi, const QBrush & block_bkg);
    void on_BlockNumber_changed(int li);
    int showMsg();

  private:
    Ui::MainWindow * ui;
    QTasksModel      model;
    bool             break_loop;
    bool             ignore_powerstatus = false;
    QReadWriteLock   power_on_lock;
    bool             power_on;
    QReadWriteLock   break_loop_lock;
    RunThread        tasks_thread;
};

#endif // MAINWINDOW_H
