#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QSerialPortInfo>

#include "com_trigger_task.h"

class SerialList
{
public:
    static QSerialPort& port(const QString & port)
    {
        static SerialList inst;

        if (inst.serial_map.find(port) == inst.serial_map.end())
        {
            inst.serial_map[port] = new QSerialPort(port);
        }
        return  *inst.serial_map[port];
    }

private:
    SerialList() {}
    ~SerialList() 
    {
        for (auto i = serial_map.begin(); i != serial_map.end(); ++i)
            if (i.value()) delete i.value();
        serial_map.clear();
    }
    SerialList(const SerialList& root) = delete;
    SerialList& operator=(const SerialList&) = delete;

    QMap<QString, QSerialPort*> serial_map;
};

ComTriggerTask::ComTriggerTask()
{
    timeout  = 10;
    set_rts  = false;
    set_dtr  = false;
    rts_hi_chb = 0;
    dtr_hi_chb = 0;
}

void ComTriggerTask::createForm(QFormLayout *layout)
{
    // port combobox
    QComboBox * port_cb = new QComboBox(layout->parentWidget());
    port_cb->setObjectName(QString::fromUtf8("port_cb"));
    connect(port_cb, SIGNAL(currentIndexChanged(int)), this, SLOT(setPort(int)));
    QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(port_cb->sizePolicy().hasHeightForWidth());
    port_cb->setSizePolicy(sizePolicy1);
    layout->addRow(new QLabel("Port: "), port_cb);

    seriallist.clear();
    port_cb->clear();
    port_cb->blockSignals(true);
    foreach(const QSerialPortInfo & info, QSerialPortInfo::availablePorts())
    {
        // add port to static map
        SerialList::port(info.portName());
        // add port name to list
        seriallist.append(info.portName());
        port_cb->addItem(info.portName());
    }
    port_cb->blockSignals(false);

    if (!seriallist.isEmpty())
    {
        if (serial_name.isEmpty())
            setPort(0);
        else
            port_cb->setCurrentText(serial_name);
    }

    // ----- RTS 
    QCheckBox * rts_chb = new QCheckBox(layout->parentWidget());
    rts_chb->setText("RTS Trigger");
    rts_chb->setChecked(set_rts);
    rts_hi_chb = new QCheckBox(layout->parentWidget());
    rts_hi_chb->setText("Invert Level of RTS");
    layout->addRow(rts_chb, rts_hi_chb);
    connect(rts_chb, SIGNAL(toggled(bool)), SLOT(setRTS(bool)));
    connect(rts_hi_chb, SIGNAL(toggled(bool)), SLOT(setInvRTS(bool)));
    connect(this, SIGNAL(toggleInvRTS(bool)), rts_hi_chb, SLOT(setChecked(bool)));
    // ----- DTR
    QCheckBox* dtr_chb = new QCheckBox(layout->parentWidget());
    dtr_chb->setText("DTR Trigger");
    dtr_chb->setChecked(set_dtr);
    dtr_hi_chb = new QCheckBox(layout->parentWidget());
    dtr_hi_chb->setText("Invert Level of DTR");
    layout->addRow(dtr_chb, dtr_hi_chb);
    connect(dtr_chb, SIGNAL(toggled(bool)), SLOT(setDTR(bool)));
    connect(dtr_hi_chb, SIGNAL(toggled(bool)), SLOT(setInvDTR(bool)));
    connect(this, SIGNAL(toggleInvDTR(bool)), dtr_hi_chb, SLOT(setChecked(bool)));

    // set levels DTR & RTS
    if (!serial_name.isEmpty())
    {
        QSerialPort& serial = SerialList::port(serial_name);
        if (serial.isOpen() == false)
            serial.open(QIODevice::ReadWrite);
        rts_hi_chb->setChecked(serial.isRequestToSend());
        dtr_hi_chb->setChecked(serial.isDataTerminalReady());
    }

    // ----- timeout
    QSpinBox* timeout_sb = new QSpinBox(layout->parentWidget());
    timeout_sb->setRange(0, INT_MAX);
    timeout_sb->setValue(timeout);
    timeout_sb->setSuffix(" ms");
    connect(timeout_sb, SIGNAL(valueChanged(int)), SLOT(setTimeout(int)));
    layout->addRow(new QLabel("Timeout: "), timeout_sb);
}

void ComTriggerTask::run()
{
    QSerialPort & serial = SerialList::port(serial_name);
    if (serial.isOpen() == false)
        serial.open(QIODevice::ReadWrite);

    // invert levels
    if (set_dtr) serial.setDataTerminalReady(!serial.isDataTerminalReady());
    if (set_rts) serial.setRequestToSend(!serial.isRequestToSend());

    msleep(timeout);

    // invert levels
    if (set_dtr) serial.setDataTerminalReady(!serial.isDataTerminalReady());
    if (set_rts) serial.setRequestToSend(!serial.isRequestToSend());
}

void ComTriggerTask::write(QJsonObject &json)
{
    json["tasktype"] = QJsonValue("ComTrigger");

    json["timeout"]  = QJsonValue(timeout);
    json["set_rts"]  = QJsonValue(set_rts);
    json["set_dtr"]  = QJsonValue(set_dtr);
    json["port"]     = QJsonValue(serial_name);
    
    if (!serial_name.isEmpty())
    {
        QSerialPort& serial = SerialList::port(serial_name);
        if (serial.isOpen() == false)
            serial.open(QIODevice::ReadWrite);
        json["inv_rts"] = QJsonValue(serial.isRequestToSend());
        json["inv_dtr"] = QJsonValue(serial.isDataTerminalReady());
    }
}

void ComTriggerTask::read(const QJsonObject & json)
{
    timeout     = json["timeout"].toInt();
    set_rts     = json["set_rts"].toBool();
    set_dtr     = json["set_dtr"].toBool();
    serial_name = json["port"].toString();

    if (!serial_name.isEmpty())
    {
        QSerialPort& serial = SerialList::port(serial_name);
        if (serial.isOpen() == false)
            serial.open(QIODevice::ReadWrite);
        serial.setRequestToSend(json["inv_rts"].toBool());
        serial.setDataTerminalReady(json["inv_dtr"].toBool());
    }
}

void ComTriggerTask::setPort(int index)
{
    serial_name = seriallist[index];

    // set levels DTR & RTS
    if (!serial_name.isEmpty())
    {
        QSerialPort& serial = SerialList::port(serial_name);
        if (serial.isOpen() == false)
            serial.open(QIODevice::ReadWrite);
        emit toggleInvRTS(serial.isRequestToSend());
        emit toggleInvDTR(serial.isDataTerminalReady());
    }
}

void ComTriggerTask::setTimeout(int t)
{
    timeout = t;
}

void ComTriggerTask::setRTS(bool s)
{
    set_rts = s;
}

void ComTriggerTask::setDTR(bool s)
{
    set_dtr = s;
}

void ComTriggerTask::setInvRTS(bool s)
{
    QSerialPort& serial = SerialList::port(serial_name);
    if (serial.isOpen() == false)
        serial.open(QIODevice::ReadWrite);

    serial.setRequestToSend(s);
}

void ComTriggerTask::setInvDTR(bool s)
{
    QSerialPort& serial = SerialList::port(serial_name);
    if (serial.isOpen() == false)
        serial.open(QIODevice::ReadWrite);

    serial.setDataTerminalReady(s);
}