#include "shmtask.h"
#include <QSpinBox>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

ShmTask::ShmTask()
{
  SignalCount = 0;
  SensCh1 = 0;
  SensCh2 = 0;
  SensCh3 = 0;
  SensCh4 = 0;

   vars["ChNum"] = QVariant(QVariant::Type::Int);
   vars["SgnlFrq"] = QVariant(QVariant::Type::Int);
   vars["SgnlCount"] = QVariant(QVariant::Type::Int);

}

ShmTask::~ShmTask()
{

}

void ShmTask::createForm(QFormLayout *layout)
{
  // vars for template
  vars_cb = new QComboBox(layout->parentWidget());
  vars_cb->addItems(global_var_names);
  vars_cb->addItems(vars.keys());
  QPushButton *add_var_btn = new QPushButton("Add Var");
  connect(add_var_btn, SIGNAL(clicked(bool)), SLOT(addVarToTemplate()));
  layout->addRow(add_var_btn, vars_cb);
  // filename_template
  filename_template_le = new QLineEdit(layout->parentWidget());
  filename_template_le->setText(filename_template);
  connect(filename_template_le, SIGNAL(textChanged(QString)), SLOT(setFilenameTemplate(QString)));
  layout->addRow(new QLabel("Output Filename: "), filename_template_le);

  // SensCh1
  QComboBox * cb = new QComboBox(layout->parentWidget());
  cb->addItem("0.2");
  cb->addItem("0.4");
  cb->addItem("0.8");
  cb->addItem("2.0");
  cb->addItem("4.0");
  cb->addItem("8.0");
  cb->addItem("20.0");
  cb->addItem("40.0");
  cb->addItem("80.0");
  cb->setCurrentIndex(SensCh1);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setSensCh1(int)));
  layout->addRow(new QLabel("Sens Ch1: "), cb);

  // SensCh2
  cb = new QComboBox(layout->parentWidget());
  cb->addItem("0.2");
  cb->addItem("0.4");
  cb->addItem("0.8");
  cb->addItem("2.0");
  cb->addItem("4.0");
  cb->addItem("8.0");
  cb->addItem("20.0");
  cb->addItem("40.0");
  cb->addItem("80.0");
  cb->setCurrentIndex(SensCh2);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setSensCh2(int)));
  layout->addRow(new QLabel("Sens Ch2: "), cb);

  // SensCh3
  cb = new QComboBox(layout->parentWidget());
  cb->addItem("0.2");
  cb->addItem("0.4");
  cb->addItem("0.8");
  cb->addItem("2.0");
  cb->addItem("4.0");
  cb->addItem("8.0");
  cb->addItem("20.0");
  cb->addItem("40.0");
  cb->addItem("80.0");
  cb->setCurrentIndex(SensCh3);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setSensCh3(int)));
  layout->addRow(new QLabel("Sens Ch3: "), cb);

  // SensCh4
  cb = new QComboBox(layout->parentWidget());
  cb->addItem("0.2");
  cb->addItem("0.4");
  cb->addItem("0.8");
  cb->addItem("2.0");
  cb->addItem("4.0");
  cb->addItem("8.0");
  cb->addItem("20.0");
  cb->addItem("40.0");
  cb->addItem("80.0");
  cb->setCurrentIndex(SensCh4);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setSensCh4(int)));
  layout->addRow(new QLabel("Sens Ch4: "), cb);

  // Signal Frequency
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setValue(SignalFrequency);
  sb->setRange(1000,1000000);
  sb->setSuffix(" Hz");
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setSgnlCount(int)));
  layout->addRow(new QLabel("Signal Frequency: "), sb);

  // Signal Count
  sb = new QSpinBox(layout->parentWidget());
  sb->setValue(SignalCount);
  sb->setRange(1,999);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setSgnlCount(int)));
  layout->addRow(new QLabel("Signal Count: "), sb);

  // output directory
  outdir_le = new LineFileBrowse(layout->parentWidget(), true);
  outdir_le->setText(out_dir);
  connect(outdir_le, SIGNAL(editingFinished()), SLOT(setOutDir()));
  layout->addRow(new QLabel("Output Directory: "), outdir_le);
}

void ShmTask::run()
{
  // generated file name in accordance with the template
  QVariantList _var_list = var_list;
  {
    QReadLocker lock(&global_vars_lock);
    substVars(_var_list, global_vars);
  }
  substVars(_var_list, vars);
  QString output_filename = composeNameFromList(_var_list);

}

void ShmTask::write(QJsonObject & json)
{
  json["tasktype"] = QJsonValue("SHM");

//  json["Ctrl_Ch"] = QJsonValue(uCtrlCh);
//  json["SetPoint"] = QJsonValue(fToSetPoint);
//  json["Rate"] = QJsonValue(fFrequency);
//  json["Waveform"] = QJsonValue((int)uWaveform);
//  json["Relative"] = QJsonValue((int)bRelative);
}

void ShmTask::read(const QJsonObject & json)
{
//  uCtrlCh = json["Ctrl_Ch"].toInt();
//  fToSetPoint = (float)json["SetPoint"].toDouble();
//  fFrequency = (float)json["Rate"].toDouble();
//  bRelative = (unsigned)json["Relative"].toInt();
//  uWaveform = (unsigned)json["Waveform"].toInt();
}

void ShmTask::setSgnlCount(int val)
{
  SignalCount = val;
}

void ShmTask::setSgnlFrq(int val)
{
  SignalFrequency = val;
}

void ShmTask::setSensCh1(int val)
{
  SensCh1 = val;
}

void ShmTask::setSensCh2(int val)
{
  SensCh2 = val;
}

void ShmTask::setSensCh3(int val)
{
  SensCh3 = val;
}

void ShmTask::setSensCh4(int val)
{
  SensCh4 = val;
}
void ShmTask::addVarToTemplate()
{
  QString var_name = vars_cb->currentText();
  filename_template_le->insert("{" + var_name + "}");
}

void ShmTask::setFilenameTemplate(const QString &val)
{
  filename_template = val;
}
