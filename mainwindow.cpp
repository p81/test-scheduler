#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMenu>
#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>
#include <QThread>
#include "MTLtask.h"

#include "canoneos.h"
#include "task.h"

//

MainWindow * MainWindow::main_window = 0;

void RunThread::run()
{
  MainWindow::main_window->run();
}

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  main_window = this;
  ui->setupUi(this);
  ui->values_tbl->setFont(QFont("Arial", 12));
  ui->values_tbl->horizontalHeader()->setFont(QFont("Arial", 12));

  ui->connect_btn->setEnabled(MTL32::isLoaded());

  createActions();
  createMenu();

  ui->task_tree->setModel(&model);
  QStringList list = Task::listTaskName();
  foreach(const QString &taskname, Task::listTaskName())
    ui->tasktype_cb->addItem(taskname);

  // mtl & global vars table init
  createTable();

  connect(&tasks_thread,
    SIGNAL(setBlockBkg(int, QBrush)),
    SLOT(on_BlockBkg_changed(int, QBrush)));
  connect(&tasks_thread,
    SIGNAL(setBlockNumber(int)),
    SLOT(on_BlockNumber_changed(int)));
  connect(&tasks_thread, 
      SIGNAL(showMsg()),
      SLOT(showMsg()),
      Qt::BlockingQueuedConnection);

  connect(ui->nbocks_sb, SIGNAL(valueChanged(int)), &model, SLOT(setNBlocks(int)));
  connect(&model, &QTasksModel::nblocksChanged, ui->nbocks_sb, &QSpinBox::setValue);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::createMenu()
{
//  // file
//  file_menu = menuBar()->addMenu(tr("&File"));
//  file_menu->addAction(open_act);
}

int MainWindow::execEndOfStep(int eostype)
{
  switch(eostype)
  {
    case 0:
    default:
      break;
    case 1:
    {
      //QMessageBox messagebox(QMessageBox::Information,
      //                       "End Of Step",
      //                       "Please click Ok to continue!",
      //                       QMessageBox::Ok | QMessageBox::Cancel);
      //return messagebox.exec();
        return emit tasks_thread.showMsg();
    }
      break;
  }
  return 0;
}

void MainWindow::createActions()
{
//  open_act = new QAction(tr("&Open..."), this);
//  open_act->setShortcuts(QKeySequence::Open);
//  open_act->setStatusTip(tr("Open an existing file"));
//  connect(open_act, SIGNAL(triggered()), this, SLOT(open()));
}

void MainWindow::open()
{
  QStringList fileNames = QFileDialog::getOpenFileNames(
        this,
        tr("Select file to open"),
        "",
        tr("Dat (*.dat);;All Data (*.*)"));

  if (fileNames.size() > 0)
  {
  }
}

void MainWindow::on_addblock_btn_clicked()
{
  model.addBlock();
}

void MainWindow::on_rem_btn_clicked()
{
  QItemSelectionModel * selection_model = ui->task_tree->selectionModel();
  if (selection_model)
  {
    QMessageBox messagebox(QMessageBox::Critical,
                           "Remove Item",
                           "Are you sure?",
                           QMessageBox::Yes | QMessageBox::No);
    if (messagebox.exec() == QMessageBox::Yes)
      model.remove(selection_model->selectedIndexes());
  }
}

void MainWindow::on_addtask_btn_clicked()
{
  model.addTask(ui->task_tree->currentIndex(), ui->tasktype_cb->currentText());
}

// click mouse on tree item
void MainWindow::on_task_tree_clicked(const QModelIndex &index)
{
  on_task_tree_activated(index);
}

// clear task form
void clearLayout(QLayout *layout)
{
    QLayoutItem *item;
    while(layout->count())
    {
        item = layout->takeAt(0);
        if (item->layout())
        {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget())
        {
            delete item->widget();
        }
        delete item;
    }
}

// press key enter on tree item
void MainWindow::on_task_tree_activated(const QModelIndex &index)
{
  // get group box layout and clear it
  QFormLayout *layout = qobject_cast<QFormLayout*>(ui->taskprop_gb->layout());
  if (layout) clearLayout(layout);

  if (!index.isValid()) return;

  QStandardItem * item = model.itemFromIndex(index);

  if (item->data(QTasksModel::TreeLevelRole).toInt() == 2)
  {
    // task item
    Task * task = (Task*)item->data(QTasksModel::ItemDataRole).value<uintptr_t>();

    // check item is 'task' or 'block'
    if (task == 0) return;
    // create properties form for task
    task->createForm(layout);
  }
  else if (item->data(QTasksModel::TreeLevelRole).toInt() == 1)
  {
    // block item
    QComboBox * cb = new QComboBox(layout->parentWidget());
    cb->setObjectName("end_of_step_cb");
    cb->addItem("Go to Next Step");
    cb->addItem("Wait for Operator");
    cb->setCurrentIndex(item->data(QTasksModel::ItemDataRole).toInt());
    connect(cb, SIGNAL(currentIndexChanged(int)),
            SLOT(end_of_step_cb_currentIndexChange(int)));
    layout->addRow(new QLabel("End of Step: "), cb);
  }
}

void MainWindow::on_start_stop_btn_clicked()
{
  if (ui->start_stop_btn->text() == "Start")
  {
    ui->start_stop_btn->setText("Stop");

    tasks_thread.start();
//    run();
  }
  else
  {
    //break_loop = true;
    breakLoop(true);
    MTL32::GDSResetControlWaveform(0);
    ui->start_stop_btn->setText("Start");
//    ui->task_tree->setEnabled(true);
  }
}

// run tasks loop
// TODO block and block cycle - need to rename
void MainWindow::run()
{
  // gets root item of Tasks
  QStandardItem *root_item = model.invisibleRootItem();
  // gets block cycles item in UI table
  int bc_index = Task::getGlobalVarNames().indexOf("Block Cycle");
  QTableWidgetItem * bc_count_item = ui->values_tbl->item(0, bc_index);

  // start cycling
  int nblocks = ui->nbocks_sb->value(); // reads number of block cycles to be performed
  int nblocks_start = bc_count_item->data(Qt::DisplayRole).toInt(); // reads start block cycle
  Task::setGlobalVar("Block Cycle", QVariant(nblocks_start)); // sets start block cycle to the appropriate global variable
  //break_loop = false;
  breakLoop(false);
  // retrives current block and sets start block
  int start_block = ui->task_tree->currentIndex().row();
  QStandardItem * currentitem = model.itemFromIndex(ui->task_tree->currentIndex());
  if (currentitem && currentitem->data(QTasksModel::TreeLevelRole).toInt() == 2)
    start_block = currentitem->parent()->row();
  if (!ui->startblock_chb->isChecked())
    start_block = 0;
  //

  int li = 0;
  for (li = nblocks_start; li < nblocks && !break_loop && (ignore_powerstatus || (!ignore_powerstatus && power_on)); Task::setGlobalVar("Block Cycle", QVariant(++li))) // cycle
  {
    // set Block Cycle in table
//    bc_count_item->setData(Qt::DisplayRole, li);
    emit tasks_thread.setBlockNumber(li);

    for (int bi = start_block; bi < root_item->rowCount() && !break_loop && (ignore_powerstatus || (!ignore_powerstatus && power_on)); ++bi) // block loop
    {
      QList<Task*> task_list;
      QStandardItem * block = root_item->child(bi);
      // change color of current block
      QBrush block_bkg = block->background();
      block->setBackground(QBrush(QColor(255,0,0)));
      emit tasks_thread.setBlockBkg(bi, QBrush(QColor(255,0,0)));
      for (int ti = 0; ti < block->rowCount(); ++ti) // task loop
      {
        QStandardItem * task_item = root_item->child(bi)->child(ti);
        Task * task = (Task*)task_item->data(QTasksModel::ItemDataRole).value<uintptr_t>();
        if (task)
        {
          task_list.push_back(task);
          task->initialize();
        }
      }
      // start tasks
      for (int ti = 0; ti < task_list.count(); ++ti)
        task_list.at(ti)->start();
      // wait for the completion of all tasks
//      for (int ti = 0; ti < task_list.count(); ++ti)
//        while (!task_list.at(ti)->isFinished())
//            QCoreApplication::processEvents();
      // wait tasks
      for (int ti = 0; ti < task_list.count(); ++ti)
        task_list.at(ti)->wait();

      // finalize
      for (int ti = 0; ti < task_list.count(); ++ti)
        task_list.at(ti)->finalize();

      // clear task list without delete task
      task_list.clear();
      // end blocks condition
      if (execEndOfStep(block->data(QTasksModel::ItemDataRole).toInt()) == QMessageBox::Cancel)
          breakLoop(true); //break_loop = true;
      // reset color of current block
      block->setBackground(block_bkg);
      emit tasks_thread.setBlockBkg(bi, block_bkg);
    }
    start_block = 0;
  }
  // set Block Cycle in table
//  bc_count_item->setData(Qt::DisplayRole, li);
  emit tasks_thread.setBlockNumber(li);
  //
  ui->start_stop_btn->setText("Start");
}

void MainWindow::on_BlockBkg_changed(int bi, const QBrush & block_bkg)
{
  QStandardItem *root_item = model.invisibleRootItem();
  QStandardItem * block = root_item->child(bi);
  block->setBackground(block_bkg);
  ui->task_tree->update(block->index());
}

void MainWindow::on_BlockNumber_changed(int li)
{
  int bc_index = Task::getGlobalVarNames().indexOf("Block Cycle");
  QTableWidgetItem * bc_count_item = ui->values_tbl->item(0, bc_index);
  // set Block Cycle in table
  bc_count_item->setData(Qt::DisplayRole, li);
}

int MainWindow::showMsg()
{
    QMessageBox messagebox(QMessageBox::Information,
        "End Of Step",
        "Please click Ok to continue!",
        QMessageBox::Ok | QMessageBox::Cancel);
    return messagebox.exec();
}

void MainWindow::breakLoop(bool b)
{
    QWriteLocker locker(&break_loop_lock);
    break_loop = b;
}

void MainWindow::on_ignorepowerstatus_chb_toggled(bool b)
{
    ignore_powerstatus = b;
}

void MainWindow::setPowerOn(bool b)
{
    QWriteLocker locker(&power_on_lock);
    power_on = b;
}

void MainWindow::updateTable()
{
  QStringList var_names =  Task::getGlobalVarNames();

  if (var_names.empty()) return ;

  int var_count = var_names.count();

  ui->values_tbl->blockSignals(true);
  if (var_count == ui->values_tbl->columnCount())
    for (int i = 0; i < var_count; ++i)
    {
      QTableWidgetItem * item = ui->values_tbl->item(0, i);
      QVariant val = Task::getGlobalVar(var_names[i]);
      item->setData(Qt::DisplayRole, val);
    }
  ui->values_tbl->blockSignals(false);

  // set the color of ui->state_frame depending on the state of machine
  int state = Task::getMachineState();
  QPalette pal;
  pal.setColor(QPalette::Background, state ? Qt::green : Qt::red);
  ui->state_frame->setAutoFillBackground(true);
  ui->state_frame->setPalette(pal);
}

void MainWindow::createTable()
{
  ui->values_tbl->clear();
  // global vars table init
  const QStringList var_names = Task::getGlobalVarNames();

  ui->values_tbl->blockSignals(true);
  ui->values_tbl->setColumnCount(var_names.count());
  ui->values_tbl->setRowCount(1);
  int i = 0;
  // global vars
  for (i = 0; i < var_names.count(); ++i)
  {
    const QString & var_name = var_names.at(i);
    QTableWidgetItem *__qheaderitem = new QTableWidgetItem();
    __qheaderitem->setText(var_name);
    ui->values_tbl->setHorizontalHeaderItem(i, __qheaderitem);
    QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
    __qtablewidgetitem->setData(Qt::DisplayRole, Task::getGlobalVar(var_names[i]));
    ui->values_tbl->setItem(0, i, __qtablewidgetitem);
  }
  ui->values_tbl->blockSignals(false);
}

void MainWindow::end_of_step_cb_currentIndexChange(int index)
{
  QStandardItem * block = model.itemFromIndex(ui->task_tree->currentIndex());
  if (!block) return;

  // set 'End of Step' for current block
  if (block->data(QTasksModel::TreeLevelRole).toInt() == 1)
    block->setData(index, QTasksModel::ItemDataRole);
}

void MainWindow::on_saveas_btn_clicked()
{
  QString filename = QFileDialog::getSaveFileName();
  if (!filename.isEmpty())
  {
    model.save(filename);
  }
}

void MainWindow::on_load_btn_clicked()
{
  QString filename = QFileDialog::getOpenFileName();
  if (!filename.isEmpty())
  {
    model.load(filename);
  }
}

void MainWindow::on_connect_btn_clicked()
{
  if (MTL32::isConnected())
  {
    MtlTask::disconnectStation();
    ui->connect_btn->setText("Connect");
  }
  else
  {
    MtlTask::connectStation(ui->station_le->text());
    if (MTL32::isConnected()) ui->connect_btn->setText("Disconnect");
  }
}


void MainWindow::on_values_tbl_cellChanged(int row, int column)
{
  QString val_name = ui->values_tbl->horizontalHeaderItem(column)->text();
  QVariant val = ui->values_tbl->item(row, column)->data(Qt::DisplayRole);
  if (val_name == "Block Cycle")
  {
    Task::setGlobalVar(val_name, val);
  }
  else if (val_name.contains("CY-Actuator"))
  {
    Task::setGlobalVar(val_name, val);
    MTL32::GDSPresetCycleCount(0, val.toInt());
  }
}
