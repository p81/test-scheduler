#ifndef MTLTASK_H
#define MTLTASK_H

#include "task.h"

#include <windows.h>

#define LOG_CHANNELS 145
#define U_WORD int
#define _export __declspec(dllimport)
#include <windows.h>
#include <winbase.h>

extern "C"
{
#include "MTL/MTL32dllH.h"
}

typedef int (__stdcall *PGDSRegisterStation)(const char *sPort, const char* sPanel, int iStation, GDSCallback lpGDSTaskCall);
typedef int (__stdcall *PGDSUnregisterStation)(void);

typedef int (__stdcall *PGDSRegisterDAQRequest)(unsigned uCtrlCh, unsigned uDataType, unsigned uStartCycCount,
                                      unsigned uNoOfCycles, unsigned uEndPts, SYSTEMTIME StartTime, unsigned uEndTicks, GDSLogProc lpCallback);
typedef int (__stdcall *PGDSRegisterVBDAQRequest)(unsigned uCtrlCh, unsigned uDataType, unsigned uStartCycCount,
                                        unsigned uNoOfCycles, unsigned uEndPts, unsigned uEndTicks, GDSLogProc lpCallback);
typedef int (__stdcall *PGDSRemoveDAQRequest)(int uIDCode);

typedef int (__stdcall *PGDSInitializePacketPointer) ( int *lpFrom );
typedef int (__stdcall *PGDSReturnVariableTypeOfLOGCh)(unsigned uCtrlCh, unsigned uCh);
typedef int (__stdcall *PGDSNextLongPoint)();
typedef float (__stdcall *PGDSNextFloatPoint)();

typedef int (__stdcall *PGDSCycleBlockControlSegment)(int uCtrlCh, int iMode, float fMean, float fAmplitude, float fFrequency,
                                            unsigned uQCycles, unsigned uQuadrant, unsigned uWaveform, BOOL bRelative,
                                            BOOL bAdaptiveControl, BOOL bConstantRate, BOOL bCycleCount, BOOL bIncBlockCount);

typedef int (__stdcall *PGDSPresetCycleCount)(int uCtrlCh, unsigned uCount);
typedef int (__stdcall *PGDSResetControlWaveform)(int uCtrlCh);
typedef int  (__stdcall *PGDSPeakValleySegment)(int uCtrlCh, float fToSetPoint, float fGain, float fFrequency, unsigned uWaveform,
                                      int bRelative, int bConstantRate);

typedef int (__stdcall *PGDSSwitchControlMode)(int uCtrlCh, int iMode);

class MTL32
{
    static bool connected;
    static HMODULE hMTL32;
    static PGDSRegisterStation           _GDSRegisterStation;
    static PGDSUnregisterStation         _GDSUnregisterStation;
    static PGDSRegisterDAQRequest        _GDSRegisterDAQRequest;
    static PGDSRemoveDAQRequest          _GDSRemoveDAQRequest;
    static PGDSInitializePacketPointer   _GDSInitializePacketPointer;
    static PGDSReturnVariableTypeOfLOGCh _GDSReturnVariableTypeOfLOGCh;
    static PGDSNextLongPoint             _GDSNextLongPoint;
    static PGDSNextFloatPoint            _GDSNextFloatPoint;
    static PGDSRegisterVBDAQRequest      _GDSRegisterVBDAQRequest;
    static PGDSCycleBlockControlSegment  _GDSCycleBlockControlSegment;
    static PGDSPresetCycleCount          _GDSPresetCycleCount;
    static PGDSResetControlWaveform      _GDSResetControlWaveform;
    static PGDSPeakValleySegment         _GDSPeakValleySegment;
    static PGDSSwitchControlMode         _GDSSwitchControlMode;


  public:
    MTL32();
    ~MTL32();

    static bool isLoaded()
    {
      return hMTL32 ? true : false;
    }

    static bool isConnected()
    {
      return connected;
    }

    static int GDSRegisterStation(const char *sPort, const char* sPanel, int iStation, GDSCallback lpGDSTaskCall)
    {
      int ret = 0;
      //GDSRegisterStation((char*)sPort, (char*)sPanel, iStation, lpGDSTaskCall);
      if (_GDSRegisterStation)
      {
        ret = _GDSRegisterStation(sPort, sPanel, iStation, lpGDSTaskCall);
        if (ret == 0)
          connected = true;
      }
      return ret;
    }

    static int GDSUnregisterStation()
    {
      int ret = 0;
      if (_GDSUnregisterStation)
        ret = _GDSUnregisterStation();
      connected = false;
      return ret;
    }

    static int GDSRegisterDAQRequest(unsigned uCtrlCh, unsigned uDataType, unsigned uStartCycCount,
                                     unsigned uNoOfCycles, unsigned uEndPts, SYSTEMTIME StartTime, unsigned uEndTicks, GDSLogProc lpCallback)
    {
      if (_GDSRegisterDAQRequest)
        return _GDSRegisterDAQRequest(uCtrlCh, uDataType, uStartCycCount,
                                      uNoOfCycles, uEndPts, StartTime, uEndTicks, lpCallback);
      else
        return 0;
    }

    static int GDSRegisterVBDAQRequest(unsigned uCtrlCh, unsigned uDataType, unsigned uStartCycCount,
                                       unsigned uNoOfCycles, unsigned uEndPts, unsigned uEndTicks, GDSLogProc lpCallback)
    {
      if (_GDSRegisterVBDAQRequest)
          return _GDSRegisterVBDAQRequest(uCtrlCh, uDataType, uStartCycCount,
                                          uNoOfCycles, uEndPts, uEndTicks, lpCallback);
      else
          return 0;
    }

    static int GDSRemoveDAQRequest(int uIDCode)
    {
      return _GDSRemoveDAQRequest ? _GDSRemoveDAQRequest(uIDCode) : 0;
    }

    static int GDSInitializePacketPointer( int *lpFrom )
    {
      return _GDSInitializePacketPointer ?
            _GDSInitializePacketPointer(lpFrom) : 0;
    }

    static int GDSReturnVariableTypeOfLOGCh(unsigned uCtrlCh, unsigned uCh)
    {
      return _GDSReturnVariableTypeOfLOGCh ?
            _GDSReturnVariableTypeOfLOGCh(uCtrlCh, uCh) : 0;
    }

    static int GDSNextLongPoint()
    {
      return _GDSNextLongPoint ? _GDSNextLongPoint() : 0;
    }

    static float GDSNextFloatPoint()
    {
      return _GDSNextFloatPoint ? _GDSNextFloatPoint() : 0;
    }

    static int GDSCycleBlockControlSegment(int uCtrlCh, int iMode, float fMean, float fAmplitude, float fFrequency,
                                           unsigned uQCycles, unsigned uQuadrant, unsigned uWaveform, BOOL bRelative,
                                           BOOL bAdaptiveControl, BOOL bConstantRate, BOOL bCycleCount, BOOL bIncBlockCount)
    {
      if (_GDSCycleBlockControlSegment)
        return _GDSCycleBlockControlSegment(uCtrlCh, iMode, fMean, fAmplitude, fFrequency,
                                           uQCycles, uQuadrant, uWaveform, bRelative,
                                           bAdaptiveControl, bConstantRate, bCycleCount, bIncBlockCount);
      else
        return 0;
    }

    static int GDSPresetCycleCount(int uCtrlCh, unsigned uCount)
    {
      if (_GDSPresetCycleCount)
          return _GDSPresetCycleCount(uCtrlCh, uCount);
      else
          return 0;
    }

    static int GDSResetControlWaveform(int uCtrlCh)
    {
        if (_GDSResetControlWaveform)
            return _GDSResetControlWaveform(uCtrlCh);
        else
            return 0;
    }

    static int GDSPeakValleySegment(int uCtrlCh, float fToSetPoint, float fGain, float fFrequency, unsigned uWaveform,
                                           int bRelative, int bConstantRate)
    {
      if (_GDSPeakValleySegment)
        return _GDSPeakValleySegment(uCtrlCh, fToSetPoint, fGain, fFrequency, uWaveform, bRelative, bConstantRate);
      else
        return 0;
    }

    static int GDSSwitchControlMode(int uCtrlCh, int iMode)
    {
      if (_GDSSwitchControlMode)
        return _GDSSwitchControlMode(uCtrlCh, iMode);
      else
        return 0;
    }
};

class LogThread : public QThread
{
  Q_OBJECT
  void run() Q_DECL_OVERRIDE;
};

class MtlTask : public Task
{
    Q_OBJECT

    static MTL32 _MTL32;
    static int instance_mtl_task_count;
    static int log_channels;
    static int daq;
    static LogThread  log_thread;
  public:
    static QQueue<QVariantList> log_buffer;
    static QReadWriteLock log_buffer_lock;
  public:
    MtlTask();
    ~MtlTask();
//    virtual void createForm(QFormLayout *layout);
    static int __stdcall _GDSSlowCallback(CTRL_CHANNEL_STATUS *CCS);
    static int __stdcall _GDSFastCallback(unsigned uIDCode,
                                          U_WORD *Data,
                                          unsigned uCols,
                                          unsigned uPoints);
    static void connectStation(const QString & station);
    static void disconnectStation();
  protected slots:
  private:
};

#endif // MTLTASK_H
