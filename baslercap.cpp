// baslercap.cpp
// Include files to use the PYLON API.

#include "baslercap.h"
using namespace Pylon;
using namespace std;

QMap<QString, CDeviceInfo>  BaslerCamera::cam_id_map;
QReadWriteLock BaslerCamera::map_lock;
QTimer BaslerCamera::update_timer;
int BaslerCamera::ref_count = 0;

BaslerCamera::BaslerCamera(CImageEventHandler * eh)
  : configuration(0),
    camera(0),
    mode(kBaslerSoftwareTrigger),
    event_handler(eh)
{
  if (ref_count == 0)
  {
    PylonInitialize();
    retriveCamList();
    connect(&update_timer, &QTimer::timeout, &BaslerCamera::retriveCamList);
    update_timer.setInterval(5000);
    update_timer.start();
  }
  ref_count++;
}

BaslerCamera::~BaslerCamera()
{
  if (camera) 
  {
    deregisterCamera();
    delete camera;
    camera = 0;
  }
  ref_count--;
  if (ref_count == 0)
  {
    PylonTerminate();
    update_timer.stop();
  }
}

void BaslerCamera::setCamera(const QString & _id)
{
  if (_id.isEmpty() || id == _id) return;

  id = _id;

  try
  {
    if (camera)
    {
      // deregister Configuration and EventHandler objects
      deregisterCamera();
      // delete camera object
      delete camera;
      camera = 0;
    }

    create();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::create()
{
  if (id.isEmpty()) return;

  try
  {
    QReadLocker locker(&map_lock);
    QMap<QString, CDeviceInfo>::iterator it = cam_id_map.find(id);
    if (it == cam_id_map.end()) return;
    CTlFactory& tlFactory = CTlFactory::GetInstance();
    camera = new CInstantCamera(tlFactory.CreateDevice(it.value()));
    registerCamera(mode);
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::registerCamera(CamMode _mode)
{
  if (camera == 0) return;

  try
  {
    mode = _mode;
    camera->RegisterImageEventHandler(event_handler, RegistrationMode_Append, Cleanup_None);

    //configuration = new  CAcquireContinuousConfiguration;
    //camera->RegisterConfiguration(configuration, RegistrationMode_ReplaceAll, Cleanup_Delete);

    if (camera->IsGrabbing())
      camera->StopGrabbing();

    switch (mode)
    {
      case kBaslerContinuous:
        configuration = new  CAcquireContinuousConfiguration;
        camera->RegisterConfiguration(configuration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        break;
      case kBaslerSoftwareTrigger:
        configuration = new CSoftwareTriggerConfiguration;
        camera->RegisterConfiguration(configuration, RegistrationMode_ReplaceAll, Cleanup_Delete);
        //camera->StartGrabbing(GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);
        break;
    }
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::deregisterCamera()
{
  if (camera == 0) return;

  try
  {
    if (configuration)
    {
      camera->DeregisterConfiguration(configuration);
      configuration = 0;
    }

    camera->DeregisterImageEventHandler(event_handler);
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

QStringList BaslerCamera::getCamNames()
{
  QReadLocker locker(&map_lock);
  return cam_id_map.keys();
}

void BaslerCamera::retriveCamList()
{
  try
  {
    QWriteLocker locker(&map_lock);
    // clear camera list
    cam_id_map.clear();
    // Get the transport layer factory.
    CTlFactory& tlFactory = CTlFactory::GetInstance();
    // Get all attached devices and exit application if no device is found.
    DeviceInfoList_t devices;
    tlFactory.EnumerateDevices(devices);

    // fill cam_list
    for (int i = 0; i < devices.size(); ++i)
    {
      QString _cam_name = QString("%1 %2 %3").arg(devices[i].GetDeviceClass().c_str())
        .arg(devices[i].GetModelName().c_str())
        .arg(devices[i].GetSerialNumber().c_str());

      cam_id_map[_cam_name] = devices[i];
    }
  }
  catch (const GenericException &e)
  {
    //emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::loadSettings(const QString & filename)
{
  if (camera == 0) return;
  try
  {
    if (!camera->IsOpen()) camera->Open();
    if (!camera->IsCameraDeviceRemoved())
      CFeaturePersistence::Load(filename.toLocal8Bit().constData(), &camera->GetNodeMap(), true);
    camera->Close();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::setMode(CamMode _mode)
{
  if (mode == _mode) return;

  deregisterCamera();
  registerCamera(_mode);
}

void BaslerCamera::setFPS(float fps)
{
  if (camera == 0) return;

  try
  {
//    if (!camera->IsOpen()) camera->Open();
    //camera->AcquisitionFrameRateEnable.SetValue(true);
    //camera->AcquisitionFrameRateAbs.SetValue(fps);
    GENAPI_NAMESPACE::INodeMap & nodemap = camera->GetNodeMap();
    GENAPI_NAMESPACE::INode * nodeAcquisitionFrameRateEnable = nodemap.GetNode("AcquisitionFrameRateEnable");
    if (nodeAcquisitionFrameRateEnable) GENAPI_NAMESPACE::CBooleanPtr(nodeAcquisitionFrameRateEnable)->SetValue(true);
    GENAPI_NAMESPACE::INode * nodeAcquisitionFrameRateAbs = nodemap.GetNode("AcquisitionFrameRateAbs");
    if (nodeAcquisitionFrameRateAbs) GENAPI_NAMESPACE::CFloatPtr(nodeAcquisitionFrameRateAbs)->SetValue(fps);
//    camera->Close();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::open()
{
  if (camera == 0) return;

  try
  {
//    if (!camera->IsOpen())
    camera->Open();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::close()
{
  if (camera == 0) return;

  try
  {
//    if (!camera->IsOpen())
    camera->Close();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

//void BaslerCamera::wait()
//{
//  if (camera == 0) return;
//
//  try
//  {
//    if (camera->IsGrabbing())
//      camera->GetGrabStopWaitObject().Wait(5000);
//  }
//  catch (const GenericException &e)
//  {
//    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
//  }
//}

void BaslerCamera::executeTrigger()
{
  if (camera == 0) return;

  try
  {
    if (camera->WaitForFrameTriggerReady(5000, TimeoutHandling_Return))
      camera->ExecuteSoftwareTrigger();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::startGrabbing(int frames)
{
  if (camera == 0) return;

  try
  {
    if (frames > 0)
      camera->StartGrabbing(frames, GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);
    else
      camera->StartGrabbing(GrabStrategy_OneByOne, GrabLoop_ProvidedByInstantCamera);
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}

void BaslerCamera::stopGrabbing()
{
  if (camera == 0) return;

  try
  {
    camera->StopGrabbing();
  }
  catch (const GenericException &e)
  {
    emit exceptionOccurred(QString("An exception occurred.\n%1").arg(e.GetDescription()));
  }
}
