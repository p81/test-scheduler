#include <vector>
#include <string>
#include <QDir>
#include <QObject>
#include <EDSDK.h>
#include <mainwindow.h>
#include "canoneos.h"

void XOR8(const unsigned char *x, size_t len, unsigned char & hex)
{
  unsigned char h = 0;

  for (int i = 0; i < len; ++i)
    h ^= x[i];

  hex = h;
}

void CanonEOSUpdater::update()
{
  CanonEOS::retriveCamList();
}

CanonEOSThread * CanonEOSThread::self = 0;
int CanonEOSThread::ref_count = 0;

CanonEOSThread * CanonEOSThread::instance()
{
  if (!self)
  {
    self = new CanonEOSThread();
    connect(&self->update_timer, &QTimer::timeout, &self->updater, &CanonEOSUpdater::update);
    self->updater.moveToThread(self);
    self->start();
    self->update_timer.start(5000);
  }
  ref_count++;
  return self;
}

bool CanonEOSThread::deleteInstance()
{
  if (ref_count)
    ref_count--;

  if (self && ref_count == 0)
  {
    self->update_timer.stop();
    self->quit();
    self->wait();
    delete self;
    self = 0;
    return true;
  }
  return false;
}

CanonEOSThread::CanonEOSThread()
{
}

CanonEOSThread::~CanonEOSThread() 
{
}

void CanonEOSThread::run()
{
  // Initialize SDK
  EdsError err = EdsInitializeSDK();
  updater.update();
  // retrieve cam list
  exec();
  err = EdsTerminateSDK();
}

QReadWriteLock   CanonEOS::map_lock;
QMap<QString, QSharedPointer<CanonEOS::CanonCamera> > CanonEOS::cam_map;
QMap<QString, QSharedPointer<CanonEOS::CanonCamera> > CanonEOS::reconnect_map;
QMap<QString, QString> CanonEOS::cam_names;

QMap<QString, CanonEOS *> CanonEOS::queue;


CanonEOS::CanonEOS()
  : canon_thread(*(CanonEOSThread::instance()))
{
  moveToThread(&canon_thread);

  pixels = 0;
  max_pixels_size = 0;
  pixels_size = 0;
}

CanonEOS::~CanonEOS()
{
  unregisterCamera();
  CanonEOSThread::deleteInstance();
}

EdsError CanonEOS::handleObjectEvent(EdsObjectEvent event,
                                     EdsBaseRef     object,
                                     EdsVoid *      context)
{
  switch (event)
  {
    case kEdsObjectEvent_DirItemRequestTransfer:
    {
      CanonCamera * camera = static_cast<CanonCamera *>(context);
      QMap<QString, CanonEOS *>::iterator cam_it = queue.find(camera->id);
      if (cam_it != queue.end())
        cam_it.value()->downloadImage(object);
      break;
    }
    default:
      break;
  }

  // Object must be released
  if (object)
  {
    EdsRelease(object);
  }

  return 0;
}

EdsError CanonEOS::handlePropertyEvent(EdsPropertyEvent  event,
                                       EdsPropertyID     property,
                                       EdsUInt32         param,
                                       EdsVoid *         context)
{
  return 0;
}

EdsError CanonEOS::handleStateEvent(EdsStateEvent event,
                                    EdsUInt32     parameter,
                                    EdsVoid *     context)
{
  if (event == kEdsStateEvent_WillSoonShutDown)
  {
    CanonCamera * camera = static_cast<CanonCamera *>(context);
    EdsSendCommand(camera->ref, kEdsCameraCommand_ExtendShutDownTimer, 0);
  }
  return 0;
}


void CanonEOS::registerCamera(const QString & _id)
{
  QMap<QString, QSharedPointer<CanonEOS::CanonCamera> >::iterator cam_map_it = cam_map.find(_id);
  if (cam_map_it == cam_map.end()) return;

  unregisterCamera();

  camera = cam_map_it.value();
  camera->id = _id;

  EdsSetObjectEventHandler(camera->ref, kEdsObjectEvent_All, handleObjectEvent, camera.data());
  EdsSetCameraStateEventHandler(camera->ref, kEdsStateEvent_All, handleStateEvent, camera.data());
  EdsSetPropertyEventHandler(camera->ref, kEdsPropertyEvent_All, handlePropertyEvent, camera.data());
  camera->is_register = true;
}

void CanonEOS::unregisterCamera()
{
  if (camera.isNull()) return;

  EdsSetObjectEventHandler(camera->ref, kEdsObjectEvent_All, NULL, NULL);
  EdsSetCameraStateEventHandler(camera->ref, kEdsStateEvent_All, NULL, NULL);
  EdsSetPropertyEventHandler(camera->ref, kEdsPropertyEvent_All, NULL, NULL);

  camera->is_register = false;
}

bool CanonEOS::fillCameraList(QMap<QString, QSharedPointer<CanonEOS::CanonCamera> > & cam_map_loc, QMap<QString, QString> & names)
{
  cam_map_loc.clear();
  names.clear();

  EdsError err = EDS_ERR_OK;
  EdsCameraListRef cameraList = NULL;
  EdsUInt32 count = 0;

  // Get camera list
  err = EdsGetCameraList(&cameraList);
  // Get number of cameras
  if (err == EDS_ERR_OK)
  {
    err = EdsGetChildCount(cameraList, &count);
    if (count == 0)
      err = EDS_ERR_DEVICE_NOT_FOUND;
  }

  // fills cam_map
  if (err == EDS_ERR_OK)
  {
    for (int i = 0; i < count; ++i)
    {
      EdsCameraRef camera_ref = NULL;
      err = EdsGetChildAtIndex(cameraList, i, &camera_ref);
      EdsDeviceInfo info;
      EdsGetDeviceInfo(camera_ref, &info);

      unsigned char cam_hex;
      XOR8((uchar*)info.szPortName, EDS_MAX_NAME, cam_hex);

      QSharedPointer<CanonEOS::CanonCamera> camera(new CanonEOS::CanonCamera);
      camera->name = QString("%1 %2").arg(info.szDeviceDescription).arg(cam_hex);
      camera->ref = camera_ref;
      QString cam_id = QString("%1 %2").arg(info.szDeviceDescription).arg(info.szPortName);
      cam_map_loc.insert(cam_id, camera);
      names.insert(cam_id, camera->name);
    }
  }
  // Release camera list
  if (cameraList != NULL)
  {
    EdsRelease(cameraList);
    cameraList = NULL;
  }

  return (err == 0);
}

void CanonEOS::retriveCamList()
{
  QMap<QString, QString> names;
  QMap<QString, QSharedPointer<CanonEOS::CanonCamera> >  cam_map_loc;
  fillCameraList(cam_map_loc, names);

  for (auto it = cam_map_loc.begin(); it != cam_map_loc.end(); ++it)
  {
    // item in old cam_map
    auto old = cam_map.find(it.key());
    if (old != cam_map.end())
    {
      //EdsRelease(old.value()->ref);
      it.value() = old.value();
    }
    else
    {
      it.value()->is_open = false;
      // if not in reconnect_map then add
      if (!reconnect_map.contains(it.key()))
        reconnect_map.insert(it.key(), it.value());
    }
  }

  // TODO iterate reconnect_map

  map_lock.lockForWrite();
  cam_map = cam_map_loc;
  cam_names = names;
  map_lock.unlock();
}

QMap<QString, QString> CanonEOS::getCamNames()
{
  QReadLocker locker(&map_lock);
  return cam_names;
}

void CanonEOS::openSession(bool open)
{
  if (camera.isNull()) return;

  if (open == false && camera->is_open )
  {
    EdsCloseSession(camera->ref);
    camera->is_open = false;
  }

  if (open && camera->is_open == false)
  {
    EdsOpenSession(camera->ref);
    EdsUInt32 saveTo = kEdsSaveTo_Host;
    EdsSetPropertyData(camera->ref, kEdsPropID_SaveTo, 0, sizeof(saveTo), &saveTo);
    EdsCapacity capacity = { 0x7FFFFFFF, 0x1000, 1 };
    EdsSetCapacity(camera->ref, capacity);
    camera->is_open = true;
  }
}

void CanonEOS::takePhoto()
{
  openSession(true);
  // ��������� ���� <id, CanonEOS> � queue, ����� ����� ������ � ������� CanonEOS �� ����������� ������� ��������� ������ handleObjectEvent
  queue.insert(camera->id, this);
  // ���������� ������� �� ������ �����
  EdsSendCommand(camera->ref, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_Completely_NonAF);
  EdsSendCommand(camera->ref, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_OFF);
}

void CanonEOS::downloadImage(EdsDirectoryItemRef directoryItem)
{
  EdsError err = EDS_ERR_OK;
  EdsStreamRef streamPix = NULL;
  EdsDirectoryItemInfo dirItemInfoPix;

  err = EdsGetDirectoryItemInfo(directoryItem, &dirItemInfoPix);

  if (err == EDS_ERR_OK)
  {
    err = EdsCreateMemoryStream(dirItemInfoPix.size, &streamPix);
  }

  // download image
  if (err == EDS_ERR_OK)
  {
    err = EdsDownload(directoryItem, dirItemInfoPix.size, streamPix);
  }

  EdsUInt32 length;
  EdsGetLength(streamPix, &length);

  int _phPixelsSize = length;
  unsigned char * _phPixels = 0;

  if (length > 0)
  {
    EdsGetPointer(streamPix, (EdsVoid**)&_phPixels);
  }

  if (_phPixels && _phPixelsSize)
  {
    //    cam->image = QImage::fromData(pixels, pixels_size);
    if (max_pixels_size < _phPixelsSize)
    {
      if (pixels) delete[] pixels;
      pixels = new uchar[_phPixelsSize];
      max_pixels_size = _phPixelsSize;
    }
    pixels_size = _phPixelsSize;
    memcpy(pixels, _phPixels, pixels_size);
  }

  // complete download
  if (err == EDS_ERR_OK)
  {
    err = EdsDownloadComplete(directoryItem);
  }

  if (streamPix != NULL)
  {
    EdsRelease(streamPix);
    streamPix = NULL;
  }

  openSession(false);

  emit downloadCompleted(pixels, pixels_size);
}