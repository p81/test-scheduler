#ifndef BLOCK_TEMPLATE_H
#define BLOCK_TEMPLATE_H

#include "task.h"

// class TemplateTask
// example of implementation task class
//
class TemplateTask : public Task
{
    Q_OBJECT

    // properties
    int int_property;
    double dbl_property;
  public:
    TemplateTask();
    // form
    virtual void createForm(QFormLayout *layout);
    // execute task
    virtual void run();
    // read/write
    virtual void write(QJsonObject &json);
    virtual void read(const QJsonObject & json);
  protected slots:
    // slots for setting properties
    void setIntProperty(int val);
    void setDblProperty(double val);
};

#endif // BLOCK_TEMPLATE_H
