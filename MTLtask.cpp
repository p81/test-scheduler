#include <iostream>
//#include <sys/time.h>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QQueue>
#include <QDebug>
#include "mainwindow.h"

#include "MTLtask.h"

// class MTL32
bool MTL32::connected = false;
HMODULE MTL32::hMTL32 = 0;
PGDSRegisterStation           MTL32::_GDSRegisterStation           = 0;
PGDSUnregisterStation         MTL32::_GDSUnregisterStation         = 0;
PGDSRegisterDAQRequest        MTL32::_GDSRegisterDAQRequest        = 0;
PGDSRegisterVBDAQRequest      MTL32::_GDSRegisterVBDAQRequest      = 0;
PGDSRemoveDAQRequest          MTL32::_GDSRemoveDAQRequest          = 0;
PGDSInitializePacketPointer   MTL32::_GDSInitializePacketPointer   = 0;
PGDSReturnVariableTypeOfLOGCh MTL32::_GDSReturnVariableTypeOfLOGCh = 0;
PGDSNextLongPoint             MTL32::_GDSNextLongPoint             = 0;
PGDSNextFloatPoint            MTL32::_GDSNextFloatPoint            = 0;
PGDSCycleBlockControlSegment  MTL32::_GDSCycleBlockControlSegment  = 0;
PGDSPresetCycleCount          MTL32::_GDSPresetCycleCount          = 0;
PGDSResetControlWaveform      MTL32::_GDSResetControlWaveform      = 0;
PGDSPeakValleySegment         MTL32::_GDSPeakValleySegment         = 0;
PGDSSwitchControlMode         MTL32::_GDSSwitchControlMode         = 0;

MTL32::MTL32()
{
  hMTL32 = LoadLibrary("MTL32Calls.dll");
  _GDSRegisterStation    = (PGDSRegisterStation) GetProcAddress(hMTL32, "GDSRegisterStation");
  _GDSUnregisterStation  = (PGDSUnregisterStation) GetProcAddress(hMTL32, "GDSUnregisterStation");
  _GDSRegisterDAQRequest = (PGDSRegisterDAQRequest) GetProcAddress(hMTL32, "GDSRegisterDAQRequest");
  _GDSRegisterVBDAQRequest = (PGDSRegisterVBDAQRequest) GetProcAddress(hMTL32, "GDSRegisterVBDAQRequest");
  _GDSRemoveDAQRequest   = (PGDSRemoveDAQRequest)  GetProcAddress(hMTL32, "GDSRemoveDAQRequest");

  _GDSInitializePacketPointer   = (PGDSInitializePacketPointer) GetProcAddress(hMTL32, "GDSInitializePacketPointer");
  _GDSReturnVariableTypeOfLOGCh = (PGDSReturnVariableTypeOfLOGCh) GetProcAddress(hMTL32, "GDSReturnVariableTypeOfLOGCh");
  _GDSNextLongPoint             = (PGDSNextLongPoint) GetProcAddress(hMTL32, "GDSNextLongPoint");
  _GDSNextFloatPoint            = (PGDSNextFloatPoint)GetProcAddress(hMTL32, "GDSNextFloatPoint");

  _GDSCycleBlockControlSegment  = (PGDSCycleBlockControlSegment)GetProcAddress(hMTL32, "GDSCycleBlockControlSegment");
  _GDSPresetCycleCount          = (PGDSPresetCycleCount) GetProcAddress(hMTL32, "GDSPresetCycleCount");
  _GDSResetControlWaveform      = (PGDSResetControlWaveform) GetProcAddress(hMTL32, "GDSResetControlWaveform");
  _GDSPeakValleySegment         = (PGDSPeakValleySegment) GetProcAddress(hMTL32, "GDSPeakValleySegment");
  _GDSSwitchControlMode         = (PGDSSwitchControlMode) GetProcAddress(hMTL32, "GDSSwitchControlMode");

}

MTL32::~MTL32()
{
  MtlTask::disconnectStation();
  FreeLibrary(hMTL32);
}
// end class MTL32


void LogThread::run()
{
  MtlTask::log_buffer_lock.lockForRead();
  bool write_log = !MtlTask::log_buffer.isEmpty();
  MtlTask::log_buffer_lock.unlock();

  FILE * pfile = fopen("out.txt", "at");
  QList<double> time_stamps;

  while (true)
  {
    MtlTask::log_buffer_lock.lockForRead();
    write_log = !MtlTask::log_buffer.isEmpty();
    QVariantList buffer_line = write_log ? MtlTask::log_buffer.dequeue() : QVariantList();
    MtlTask::log_buffer_lock.unlock();

    if (!write_log) continue;

    if (buffer_line.count() == 1)
    {
      if (time_stamps.count() < 2)
      {
        time_stamps << buffer_line[0].toDouble();
      }
      else
      {
        time_stamps[0] = time_stamps[1];
        time_stamps[1] = buffer_line[0].toDouble();
        buffer_line[0] = QVariant(time_stamps[1] - time_stamps[0]);
      }
    }

    for (int i=0; i<buffer_line.count(); i++)
    {
      QVariant val = buffer_line[i];
      switch (val.type())
      {
        case QVariant::Int:
          fprintf(pfile, "%d", val.toInt());
          break;
        case QVariant::Double:
        case QMetaType::Float:
          fprintf(pfile, "%f", val.toDouble());
          break;
      }
      if (i < buffer_line.count()-1) fprintf(pfile, "\t");
    }
    fprintf(pfile, "\n");
  }

  fclose(pfile);
}

// class MtlTask
MTL32 MtlTask::_MTL32;
int MtlTask::instance_mtl_task_count = 0;
int MtlTask::log_channels = 0;
int MtlTask::daq = 0;
QQueue<QVariantList> MtlTask::log_buffer;
QReadWriteLock MtlTask::log_buffer_lock;
LogThread  MtlTask::log_thread;

MtlTask::MtlTask()
{
  if (instance_mtl_task_count == 0)
  {
  }
  instance_mtl_task_count++;
}

MtlTask::~MtlTask()
{
    instance_mtl_task_count--;
    // Releases all pylon resources
    if (instance_mtl_task_count == 0)
    {
    }
}

int MtlTask::_GDSSlowCallback(CTRL_CHANNEL_STATUS *CCS)
{
  if (!CCS) return 0;

  if (log_channels != CCS->uLogChannels)
  {
      global_vars_lock.lockForWrite();
      global_vars.clear();
      global_var_names.clear();

      for(int i = 0; i < CCS->uLogChannels; i++)
      {
        QString chName = QString(CCS->sChID[i])/* + " " + QString(CCS->sChUnits[i])*/;

        if (chName.contains("CY-Actuator"))
            global_vars[chName] = QVariant((int)CCS->fReadout[i]);
        else
            global_vars[chName] = QVariant(CCS->fReadout[i]);

        global_var_names.push_back(chName);
      }
      global_var_names.append(initGlobalVars());
      global_vars_lock.unlock();

      log_channels = CCS->uLogChannels;
      MainWindow::main_window->createTable();
  }
  else
  {
      QWriteLocker locker(&global_vars_lock);
      for(int i = 0; i < CCS->uLogChannels; i++)
      {
        QString chName = QString(CCS->sChID[i]);
//        global_vars[chName] = QVariant(CCS->fReadout[i]);
        if (chName.contains("CY-Actuator"))
            global_vars[chName] = QVariant((int)CCS->fReadout[i]);
        else
            global_vars[chName] = QVariant(CCS->fReadout[i]);
      }
  }

  {
    QWriteLocker locker(&machine_state_lock);
    machine_state = CCS->bSGIdle;
  }

  MainWindow::main_window->setPowerOn(CCS->bPowerOn);
  MainWindow::main_window->updateTable();
  return 0;
}

int MtlTask::_GDSFastCallback(unsigned uIDCode,
                              U_WORD *Data,
                              unsigned uCols,
                              unsigned uPoints)
{
  struct timeval tp;
//  gettimeofday(&tp, NULL);
  double time_stamp = tp.tv_sec*1000000.0 + (double)tp.tv_usec;

  QWriteLocker locker(&log_buffer_lock);
  log_buffer << (QVariantList() << QVariant(time_stamp));

  int iRet = MTL32::GDSInitializePacketPointer(Data);

  for (uint j = 0; j < uPoints; j++)
  {
//      QWriteLocker locker(&global_vars_lock);
    QVariantList point_buffer;

      for (uint i = 0; i < uCols; i++)
      {
          int t = 0;

          // �������� ��� ������������ ������
          t = MTL32::GDSReturnVariableTypeOfLOGCh(0, i);

          if(t == 3)
          {
              int  val = MTL32::GDSNextLongPoint();
              point_buffer << QVariant(val);
//              if (i < global_var_names.count())
//                  global_vars[global_var_names[i]] = QVariant(val);
          }
          else
          {
              float val = MTL32::GDSNextFloatPoint();
              point_buffer << QVariant(val);
//              if (i < global_var_names.count())
//                  global_vars[global_var_names[i]] = QVariant(val);
          }
      }
      log_buffer << point_buffer;
  }
  return 0;
}

void MtlTask::connectStation(const QString & station)
{
//  log_buffer.clear();
//  if (MTL32::isLoaded())
//    MtlTask::log_thread.start(QThread::LowestPriority);
  // register station
  int ret = MTL32::GDSRegisterStation(station.toStdString().c_str(),
                                      "Demo",
                                      0,
                                      _GDSSlowCallback);

  if (ret) return;

  // register fast callback
//  SYSTEMTIME StTm;
//  GetSystemTime(&StTm);
//  daq = MTL32::GDSRegisterDAQRequest(0, LOG_STREAM, LOG_START_NOW, 0, 0, StTm, 0, _GDSFastCallback);
//  daq = MTL32::GDSRegisterVBDAQRequest(0, LOG_STREAM+LOG_START_NOW, 0, 0, 0, 0, _GDSFastCallback);
}

void MtlTask::disconnectStation()
{
  if (!MTL32::isConnected()) return ;

//  MTL32::GDSRemoveDAQRequest(daq);
  MTL32::GDSUnregisterStation();
//  MtlTask::log_thread.terminate();
//  log_buffer.clear();
}

//void MtlCycleTask::createForm(QFormLayout *layout)
//{
//}

//void MtlCycleTask::run()
//{
//}
