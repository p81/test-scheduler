#ifndef SHMTASK_H
#define SHMTASK_H

#include <QComboBox>
#include <QLineEdit>
#include "task.h"
#include "linefilebrowse.h"

class ShmTask : public Task
{

    Q_OBJECT
  typedef struct CONF
  {
    const char* file1;
    const char* file2;
    const char* file3;
    const char* file4;
    const char* file5;
    const char* file6;
    const char* file7;
    const char* file8;
    double sens1;
    double sens2;
    double sens3;
    double sens4;
    double sens5;
    double sens6;
    double sens7;
    double sens8;
    int sgnl_count;
    int sgnl_length;
  } capture_config;


    int SignalCount;
    int SignalFrequency;
    int SensCh1;
    int SensCh2;
    int SensCh3;
    int SensCh4;

    //capture_config conf;

    QString filename_template;

    QMap<QString,QVariant>  vars;
    QComboBox * vars_cb;
    QLineEdit * filename_template_le;
    QVariantList var_list;
    // output directory
    QString out_dir;
    LineFileBrowse * outdir_le;

public:
    ShmTask();
    ~ShmTask();
    virtual void createForm(QFormLayout *layout);

protected:
    virtual void run();
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);

protected slots:
  void setSgnlCount(int val);
  void setSgnlFrq(int val);
  void setSensCh1(int val);
  void setSensCh2(int val);
  void setSensCh3(int val);
  void setSensCh4(int val);

  void addVarToTemplate();
  void setFilenameTemplate(const QString &val);
};

#endif // SHMTASK_H
