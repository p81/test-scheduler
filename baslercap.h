#ifndef BASLERCAP_H
#define BASLERCAP_H

#include <QObject>
#include <QMap>
#include <QString>
#include <QTimer>
#include <QReadWriteLock>
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
using namespace Pylon;
using namespace Basler_GigECameraParams;

enum CamMode { kBaslerContinuous, kBaslerSoftwareTrigger };

class BaslerCamera : public QObject
{
    Q_OBJECT
  public:
    BaslerCamera(CImageEventHandler * eh);
    ~BaslerCamera();

    static QStringList getCamNames();
    void loadSettings(const QString & filename);
    void setMode(CamMode _mode);
    void setFPS(float fps);

  public slots:
    void setCamera(const QString & _id);
    void open();
    void close();
    //void wait();
    void executeTrigger();
    void startGrabbing(int frames = 0);
    void stopGrabbing();

  private:
    QString id;
    CInstantCamera * camera;
    CConfigurationEventHandler * configuration;
    CImageEventHandler * event_handler;
    CamMode mode;

    static QMap<QString, CDeviceInfo>  cam_id_map;
    static QReadWriteLock map_lock;
    static QTimer update_timer;

    void create();
    void registerCamera(CamMode _mode);
    void deregisterCamera();

    static void retriveCamList();
    static int ref_count;
  signals:
    void exceptionOccurred(const QString &);
};

#endif // BASLERCAP_H
