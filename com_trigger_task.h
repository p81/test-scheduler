#ifndef COM_TRIGGER_TASK_H
#define COM_TRIGGER_TASK_H

#include <QSerialPort>
#include <QCheckBox>
#include "task.h"

// class ComTriggerTask
// 
//
class ComTriggerTask : public Task
{
    Q_OBJECT

    // properties
    int        timeout;
    bool       set_rts;
    bool       set_dtr;
    QString    serial_name;
    // controls
    QCheckBox* rts_hi_chb;
    QCheckBox* dtr_hi_chb;
  public:
    ComTriggerTask();
    // form
    virtual void createForm(QFormLayout *layout);
    // execute task
    virtual void run();
    // read/write
    virtual void write(QJsonObject &json);
    virtual void read(const QJsonObject & json);
  private:
    QStringList      seriallist;
  private slots:
    void setPort(int index);
    void setTimeout(int t);
    void setRTS(bool s);
    void setDTR(bool s);
    void setInvRTS(bool s);
    void setInvDTR(bool s);
  signals:
    void toggleInvRTS(bool s);
    void toggleInvDTR(bool s);
};

#endif // COM_TRIGGER_TASK_H
