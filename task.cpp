#include <QMimeData>
#include <QString>
#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include "task_template.h"
#include "MTL_cycle.h"
#include "MTL_ramp.h"
#include "basler.h"
#include "shmtask.h"
#include "timeout_task.h"
#include "Canontask.h"
#include "com_trigger_task.h"

#include "task.h"

// class Task
QMap<QString,QVariant> Task::global_vars;
QReadWriteLock         Task::global_vars_lock;
QStringList            Task::global_var_names = Task::initGlobalVars();
int                    Task::machine_state = Task::kIdle;
QReadWriteLock         Task::machine_state_lock;
int                    Task::mtltask_state = Task::kIdle;
QReadWriteLock         Task::mtltask_state_lock;

Task::Task()
{
}

// class fabric
Task* Task::create(const QString & task_type)
{
  if (task_type == "Template")
    return new TemplateTask();
  else if (task_type == "MTLCycle")
    return new MtlCycleTask();
  else if (task_type == "MTLRamp")
    return new MtlRampTask();
  else if (task_type == "Basler")
    return new BaslerTask();
  else if (task_type == "Canon")
    return new CanonTask();
  else if (task_type == "SHM")
    return new ShmTask();
  else if (task_type == "Timeout")
    return new TimeoutTask();
  else if (task_type == "ComTrigger")
      return new ComTriggerTask();
  return new Task();
}

QStringList Task::listTaskName()
{
  QStringList list;
  list << "Template";
  list << "MTLCycle";
  list << "MTLRamp";
  list << "Basler";
  list << "Canon";
  list << "SHM";
  list << "Timeout";
  list << "ComTrigger";

  return list;
}

void Task::createForm(QFormLayout *layout)
{
}

void Task::run()
{
}

void Task::setGlobalVar(const QString &index, QVariant val)
{
  QWriteLocker locker(&global_vars_lock);
  global_vars[index] = val;
}

const QVariant Task::getGlobalVar(const QString & index)
{
  QReadLocker locker(&global_vars_lock);
  return global_vars[index];
}

const QStringList Task::getGlobalVarNames()
{
//  return mtl_vars ? mtl_vars->keys() : QStringList();
  return global_var_names;
}

const int Task::getMachineState()
{
  QReadLocker locker(&machine_state_lock);
  return machine_state;
}

const int Task::getMtlTaskState()
{
  QReadLocker locker(&mtltask_state_lock);
  return mtltask_state;
}

const void Task::setMtlTaskState(MachineState state)
{
  QWriteLocker locker(&mtltask_state_lock);
  mtltask_state = state;
}

QStringList Task::initGlobalVars()
{
  global_vars["Block Cycle"] = QVariant(QVariant::Type::Int);
  QStringList list;
  list << "Block Cycle";

  return list;
}

void Task::splitTemplateName(const QString & templ, QVariantList & templ_items)
{
  templ_items.clear();

  QRegExp rx("(\\{[\\w-#.\\s]+\\}|[\\w~!@#$%^&-=+\\s]+)");
  int pos = 0;
  while ((pos = rx.indexIn(templ, pos)) != -1)
  {
    templ_items << rx.cap(1);
    pos += rx.matchedLength();
  }
}

void Task::substVars(QVariantList & templ_items, QMap<QString,QVariant> &vars )
{
  for (int i = 0; i < templ_items.count(); ++i)
  {
    QVariant & item = templ_items[i];
    if (item.type() == QVariant::Type::String)
    {
      QString str = item.toString();
      if (str.at(0) == '{')
      {
        QString var_name = str.mid(1, str.length() - 2);
        QMap<QString,QVariant> ::iterator it = vars.find(var_name);
        if (it!=vars.end())
          item = it.value();
      }
    }
  }
}

QString Task::composeNameFromList(QVariantList & templ_items)
{
  QString output;
  for (int i = 0; i < templ_items.count(); ++i)
  {
    QVariant & item = templ_items[i];
    if (item.type() == QVariant::Double ||
        item.type() == QMetaType::Float)
      output += QString("%1").arg(item.toDouble(), 0, 'f', 3);
    else if (item.type() == QVariant::Int)
      output += QString("%1").arg(item.toInt(), 8, 10, QChar('0'));/*item.toString();*/
    else
      output += item.toString();
  }
  return output;
}

void Task::write(QJsonObject & json)
{
}

void Task::read(const QJsonObject & json)
{
}

void Task::initialize()
{
}

void Task::finalize()
{
}

// class QTasksModel
QTasksModel::~QTasksModel()
{
  QStandardItem *rootItem = invisibleRootItem();

  for (int bi = 0; bi < rootItem->rowCount(); ++bi)
  {
    QStandardItem * block = rootItem->child(bi);
    for (int ti = 0; ti < block->rowCount(); ++ti)
    {
      QStandardItem * task_item = rootItem->child(bi)->child(ti);
      Task * task = (Task*)task_item->data(QTasksModel::ItemDataRole).value<uintptr_t>();
      if (task)
        delete task;
    }
  }
}

void QTasksModel::addBlock()
{
  static int i = 0;
  QStandardItem * item = new QStandardItem(QString("b%1").arg(i));
  item->setData(1, TreeLevelRole);
  item->setData(0, ItemDataRole);
  appendRow(item);
  i++;
}

// remove items from index_list, for example selected items
void QTasksModel::remove(const QModelIndexList & index_list)
{
  QList< QPersistentModelIndex > persistentIndexList;
  for (QModelIndex const & index : index_list)
    persistentIndexList.append(index);
  for (QPersistentModelIndex const & persistentIndex : persistentIndexList)
    if (persistentIndex.isValid())
    {
      int level = persistentIndex.data(TreeLevelRole).toInt();
      if (level == 1)
      {
        QStandardItem *item = itemFromIndex(persistentIndex);
        if (item)
          for (int i = 0; i < item->rowCount(); ++i)
          {
            Task * task = (Task*)item->child(i)->data(ItemDataRole).value<uintptr_t>();
            if (task)
            {
              delete task;
            }
          }
      }
      if (level == 2)
      {
        Task * task = (Task*)persistentIndex.data(ItemDataRole).value<uintptr_t>();
        if (task)
        {
          delete task;
        }
      }
      removeRow(persistentIndex.row(), persistentIndex.parent());
    }
}

//
void QTasksModel::addTask(const QModelIndex &index, const QString & task_type)
{
  if (!index.isValid())
    return;

  // item of index type checking
  // block type or task type
  QModelIndex block_index = index;
  QStandardItem * block_item = itemFromIndex(index);
  int item_type   = block_item->data(TreeLevelRole).toInt();
  if (item_type == 2)
    block_index = block_index.parent();
  //
  static int i = 0;
  QStandardItem *item = new QStandardItem(QString("%1-%2").arg(task_type).arg(i));
  item->setData(2, TreeLevelRole);
  // create TaskData and assign with QStandardItem
  Task * task = Task::create(task_type);
  item->setData(QVariant::fromValue<uintptr_t>((uintptr_t)task), ItemDataRole);
  itemFromIndex(block_index)->appendRow(item);
  i++;
}

void QTasksModel::save(const QString & filename)
{
  QFile saveFile(filename);

  if (!saveFile.open(QIODevice::WriteOnly))
  {
    qWarning("Couldn't open save file.");
    return/* false*/;
  }

  QJsonObject rootObject;
  //
  writeRoot(rootObject);
  //
  QJsonDocument saveDoc(rootObject);
  saveFile.write(saveDoc.toJson());
}

void QTasksModel::load(const QString & filename)
{
  QFile loadFile(filename);

  if (!loadFile.open(QIODevice::ReadOnly)) {
      qWarning("Couldn't open save file.");
      return /*false*/;
  }

  clear();

  QByteArray saveData = loadFile.readAll();

  QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

  readRoot(loadDoc.object());
}

//
// Consistency checking drop items and parent (that takes)
// �������� ����������� ����� ������������ � ������������(��) ������� ������.
// ����������� ��� '�����'(�) ������ ������ ���� '����'.
// ����������� ��� '�����'(��) ������ ���� ����� ������ �������� ������.
//
bool QTasksModel::canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
  QByteArray encoded = data->data("application/x-qabstractitemmodeldatalist");
  QDataStream stream(&encoded, QIODevice::ReadOnly);

  // read data of droped item from MIME data
  int parent_type = -1;
  int drag_type   = -1;
  bool mixed_drag = false;
  while (!stream.atEnd())
  {
    int row_ = -1, col_ = -1;
    QMap<int,  QVariant> roleDataMap;
    stream >> row_ >> col_ >> roleDataMap;

    if (drag_type != -1 &&
        drag_type != roleDataMap[TreeLevelRole].toInt())
      mixed_drag = true;

    parent_type = parent.data(TreeLevelRole).toInt();
    drag_type   = roleDataMap[TreeLevelRole].toInt();
  }

  if (!mixed_drag && (drag_type == parent_type + 1))
    return true;
  return false;
}

void QTasksModel::writeRoot(QJsonObject & json)
{
  QStandardItem * root = invisibleRootItem();
  json["nblocks"] = QJsonValue(nblocks);
  int digit_number = log10(root->rowCount()) + 1;
  for (int i = 0; i < root->rowCount(); ++i)
  {
    QStandardItem * item = root->child(i);
    QJsonObject itemObject;
    writeItem(*item, itemObject);
    json[QString("%1").arg(i, digit_number, 10, QChar('0'))] = itemObject;
  }
}

void QTasksModel::writeItem(QStandardItem & item, QJsonObject & json)
{
  json["end_of_step"] = QJsonValue(item.data(QTasksModel::ItemDataRole).toInt());
  json["name"] = QJsonValue(item.data(Qt::DisplayRole).toString());
  for (int i = 0; i < item.rowCount(); ++i)
  {
    QJsonObject task_object;
    QStandardItem * task_item = item.child(i);
    Task * task = (Task*)task_item->data(ItemDataRole).value<uintptr_t>();
    task->write(task_object);
    json[task_item->data(Qt::DisplayRole).toString()] = task_object;
  }
}

void QTasksModel::readRoot(const QJsonObject & json)
{
  for (QJsonObject::const_iterator it = json.begin(); it != json.end(); ++it)
  {
    if (it.key() == "nblocks")
    {
      // read nblocks
      emit nblocksChanged(it.value().toInt());
    }
    else
    {
      // add block
      QJsonObject     item_json = it.value().toObject();
      QStandardItem * item = new QStandardItem(item_json["name"].toString());
      item->setData(1, TreeLevelRole);
      item->setData(item_json["end_of_step"].toInt(), ItemDataRole);

      readItem(item_json, *item);

      appendRow(item);
      //
    }
  }
}

void QTasksModel::readItem(const QJsonObject & json, QStandardItem &item)
{
  for (QJsonObject::const_iterator it = json.begin(); it != json.end(); ++it)
  {
    if (!it.value().isObject()) continue;

    QJsonObject task_json = it.value().toObject();

    // add task
    QStandardItem *task_item = new QStandardItem(it.key());
    task_item->setData(2, TreeLevelRole);
    // create TaskData and assign with QStandardItem
    Task * task = Task::create(task_json.value("tasktype").toString());
    task->read(task_json);
    task_item->setData(QVariant::fromValue<uintptr_t>((uintptr_t)task), ItemDataRole);
    item.appendRow(task_item);
    //
  }
}

void QTasksModel::setNBlocks(int nb)
{
  nblocks = nb;
}