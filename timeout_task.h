#ifndef TIMEOUT_TASK_H
#define TIMEOUT_TASK_H

#include "task.h"

// class TemplateTask
// example of implementation task class
//
class TimeoutTask : public Task
{
    Q_OBJECT

    // properties
    int timeout;
  public:
    TimeoutTask();
    // form
    virtual void createForm(QFormLayout *layout);
    // execute task
    virtual void run();
    // read/write
    virtual void write(QJsonObject &json);
    virtual void read(const QJsonObject & json);
  protected slots:
    // slots for setting properties
    void setTimeout(int val);
};

#endif // TIMEOUT_TASK_H
