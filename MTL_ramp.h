#ifndef MTL_RAMP_H
#define MTL_RAMP_H

#include "MTLtask.h"

class MtlRampTask : public MtlTask
{
    Q_OBJECT

    int   uCtrlCh; // Control channel (actuator No) on this station (starting from zero)
    int   iMode;   // Control Mode (0-Stroke, 1-Load or 2-Straiin) to be imposed
    float fToSetPoint; // Target point
    float fGain; // Gain Factor
    float fFrequency; // Waveform Frequency, Hz
    unsigned uWaveform; // 0-sine, 1-Ramp, 2-Pulse
    int bRelative; // if =0, specified mean is absolute. If =1, specified mean is shift from existing readout
    int bConstantRate; // if rate is to be imposed rather than frequency

    QDoubleSpinBox * dblsbSP;
    QDoubleSpinBox * dblsbRate;


  public:
    MtlRampTask();
    virtual void createForm(QFormLayout *layout);
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);
  protected:
    virtual void run();
  protected slots:
    void setuCtrlCh(int val);
    void setiMode(int val);
    void setfToSetPoint(double val);
    void setfGain(double val);
    void setfFrequency(double val);
    void setuWaveform(int val);
    void setbRelative(int val);
    void setbConstantRate(int val);

};

#endif // MTL_RAMP_H
