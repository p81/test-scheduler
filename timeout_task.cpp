#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include "timeout_task.h"

TimeoutTask::TimeoutTask()
{
  timeout = 0;
}

void TimeoutTask::createForm(QFormLayout *layout)
{
  // timeout property control
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setRange(0, INT_MAX);
  sb->setSuffix(" ms");
  sb->setValue(timeout);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setTimeout(int)));
  layout->addRow(new QLabel("Timeout: "), sb);
}

void TimeoutTask::run()
{
  Task::setMtlTaskState(Task::kBusy);
  msleep(timeout);
  Task::setMtlTaskState(Task::kIdle);
}

void TimeoutTask::write(QJsonObject &json)
{
  json["tasktype"] = QJsonValue("Timeout");

  json["timeout"] = QJsonValue(timeout);
}

void TimeoutTask::read(const QJsonObject & json)
{
  timeout = json["timeout"].toInt();
}

void TimeoutTask::setTimeout(int val)
{
  timeout = val;
}

