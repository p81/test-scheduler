#ifndef CANON_H
#define CANON_H

#include "task.h"
#include "canoneos.h"
#include "linefilebrowse.h"

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QMainWindow>
#include <QMenuBar>
#include <QPushButton>
#include <QSpinBox>
#include <QStatusBar>
#include <QTableWidget>
#include <QWidget>
#include <QMutex>
#include <QWaitCondition>

#include <QTimer>
#include <QList>


class CanonTask : public Task
{
    Q_OBJECT

    friend class ReconnectThreadCanon;

    static QStringList FileFormatExt;
    QString cam_name;
    QString cam_id;

    int     has_settings;  // 1 - load settings | 0 - doesn't load
    QString settings_file; // settings filename
    LineFileBrowse * le_settings_file;
    QString out_dir;       // output directory
    LineFileBrowse * le_out_dir;
    QString filename_template; // template for output filename
    int     multiple_shoot;  // 1 - multiple shoot mode | 0 - single shoot mode
    int     timeout; // timeout between shoots in multiple mode
    int     image_quality;
    QString image_format;
    QComboBox * cb_cam;
    QComboBox * cb_vars;
    QLineEdit * le_filename_template;
    QVariantList var_list;
    QMap<QString,QVariant>  vars;

    QMap<QString, QString> cams_map;

    bool is_test_shoot;
    int multiple_shoot_count;

    QMutex frame_mutex;
    QWaitCondition grab_result;
    CanonEOS canon_eos;
public:
    CanonTask();
    ~CanonTask();
    virtual void createForm(QFormLayout *layout);
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);
    virtual void initialize();
    virtual void finalize();
public slots:
    void takePhotoComplete(const uchar * data, int size);
protected:
    virtual void run();
protected slots:
    void setCamIndex(int val);
    void setVarCamName(const QString & val);
    void setCamSettings(int val);
    void setSettingsFile();
    void setOutDir();
    void setFilenameTemplate(const QString &val);
    void setMultipleShot(int val);
    void setTimeout(int val);
    void addVarToTemplate();
    void testShoot();
    void setFrameCount(int val);
    void setImageQuality(int val);
    void setImageFormat(const QString &val);
signals:
    void softwareShoot();
    void cameraSelected(const QString & _id);
};

#endif // CANON_H
