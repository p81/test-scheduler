#include <climits>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QMessageBox>
#include <QApplication>

#include "MTL_cycle.h"

MtlCycleTask::MtlCycleTask()
  : MtlTask()
{
  uCtrlCh = 0;
  iMode = 0;
  fMean = 0.5;
  fAmplitude = 0.5;
  fFrequency = 1;
  uQCycles = 0;
  uQuadrant = 0;
  uEndQuadrant = 0;
  uWaveform = 0;
  bRelative = 0;
  bAdaptiveControl = 0;
  bConstantRate = 0;
  // по умолчанию MTL32 ведет подсчет циклов нагружения
  bIncCycleCount = 1;
  bIncBlockCount = 0;
  // по умолчанию выбран режим ввода минимального и максимального значений цикла нагружения
  bMinMax = true;
}

void MtlCycleTask::createForm(QFormLayout *layout)
{
  // uCtrlCh
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setValue(uCtrlCh);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setuCtrlCh(int)));
  layout->addRow(new QLabel("Actuator: "), sb);
  // iMode
  QComboBox * cb = new QComboBox(layout->parentWidget());
  cb->addItem("Stroke");
  cb->addItem("Load");
  cb->addItem("Strain");
  cb->setCurrentIndex(iMode);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setiMode(int)));
  layout->addRow(new QLabel("Control Mode: "), cb);

  // Mean-Amplitude
  QRadioButton * rb = new QRadioButton(layout->parentWidget());
  rb->setChecked(true);
  rb->setAutoExclusive(true);
  layout->addRow(new QLabel("Mean-Amplitude"), rb);

  // fMean
  qdsbMean = new QDoubleSpinBox(layout->parentWidget());
  qdsbMean->setRange(-500,500);
  qdsbMean->setValue(fMean);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    qdsbMean->setSuffix(" mm");
  }
  else if(cb->currentIndex() == 1)
  {
    qdsbMean->setSuffix(" kN");
  }
  connect(qdsbMean, SIGNAL(valueChanged(double)), SLOT(setfMean(double)));
  layout->addRow(new QLabel("Mean: "), qdsbMean);
  // fAmplitude
  qdsbAmplitude = new QDoubleSpinBox(layout->parentWidget());
  qdsbAmplitude->setRange(-500,500);
  qdsbAmplitude->setValue(fAmplitude);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    qdsbAmplitude->setSuffix(" mm");
  }
  else if(cb->currentIndex() == 1)
  {
    qdsbAmplitude->setSuffix(" kN");
  }
  connect(qdsbAmplitude, SIGNAL(valueChanged(double)), SLOT(setfAmplitude(double)));
  layout->addRow(new QLabel("Amplitude: "), qdsbAmplitude);
  connect(rb, SIGNAL(toggled(bool)),qdsbMean ,SLOT(setEnabled(bool)));
  connect(rb, SIGNAL(toggled(bool)), qdsbAmplitude, SLOT(setEnabled(bool)));

  //Min-Max
  rb = new QRadioButton(layout->parentWidget());
  rb->setAutoExclusive(true);
  layout->addRow(new QLabel("Min-Max"), rb);

  // fMin
  qdsbMin = new QDoubleSpinBox(layout->parentWidget());
  qdsbMin->setRange(-500,500);
  qdsbMin->setValue(fMean-fAmplitude);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    qdsbMin->setSuffix(" mm");
  }
  else if(cb->currentIndex() == 1)
  {
    qdsbMin->setSuffix(" kN");
  }
  connect(qdsbMin, SIGNAL(valueChanged(double)), SLOT(setMin(double)));
  layout->addRow(new QLabel("Min: "), qdsbMin);
  qdsbMin->setEnabled(false);
  // fMax
  qdsbMax = new QDoubleSpinBox(layout->parentWidget());
  qdsbMax->setRange(-500,500);
  qdsbMax->setValue(fMean+fAmplitude);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    qdsbMax->setSuffix(" mm");
  }
  else if(cb->currentIndex() == 1)
  {
    qdsbMax->setSuffix(" kN");
  }
  connect(qdsbMax, SIGNAL(valueChanged(double)), SLOT(setMax(double)));
  layout->addRow(new QLabel("Max: "), qdsbMax);
  qdsbMax->setEnabled(false);
  connect(rb, SIGNAL(toggled(bool)), qdsbMin ,SLOT(setEnabled(bool)));
  connect(rb, SIGNAL(toggled(bool)), qdsbMax, SLOT(setEnabled(bool)));
  connect(rb, SIGNAL(toggled(bool)), SLOT(setbMinMax(bool)));
  rb->setChecked(bMinMax);

  // fFrequency
  QDoubleSpinBox * dblsb = new QDoubleSpinBox(layout->parentWidget());
  dblsb->setValue(fFrequency);
  dblsb->setSuffix(" Hz");
  connect(dblsb, SIGNAL(valueChanged(double)), SLOT(setfFrequency(double)));
  layout->addRow(new QLabel("Frequency: "), dblsb);
  // uQCycles
  /*QSpinBox * */sb = new QSpinBox(layout->parentWidget());
  sb->setRange(0, INT32_MAX);
  sb->setValue(uQCycles);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setuQCycles(int)));
  layout->addRow(new QLabel("Cycles: "), sb);
  // uQuadrant
  /*QComboBox * */cb = new QComboBox(layout->parentWidget());
  cb->addItem("Mean Down");
  cb->addItem("Min");
  cb->addItem("Mean Up");
  cb->addItem("Max");
  cb->setCurrentIndex(uQuadrant);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setuQuadrant(int)));
  layout->addRow(new QLabel("Start Point: "), cb);
  // uEndQuadrant
  /*QComboBox * */cb = new QComboBox(layout->parentWidget());
  cb->addItem("Mean Down");
  cb->addItem("Min");
  cb->addItem("Mean Up");
  cb->addItem("Max");
  cb->setCurrentIndex(uEndQuadrant);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setuEndQuadrant(int)));
  layout->addRow(new QLabel("End Point: "), cb);
  // uWaveform
  /*QComboBox * */cb = new QComboBox(layout->parentWidget());
  cb->addItem("Sine");
  cb->addItem("Ramp");
  cb->addItem("Pulse");
  cb->setCurrentIndex(uWaveform);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setuWaveform(int)));
  layout->addRow(new QLabel("Waveform: "), cb);
  // bRelative
//  QCheckBox * chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bRelative ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbRelative(int)));
//  layout->addRow(new QLabel("bRelative: "), chb);
  // bAdaptiveControl
  QCheckBox * chb = new QCheckBox(layout->parentWidget());
  chb->setCheckState(bAdaptiveControl ? Qt::Checked : Qt::Unchecked);
  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbAdaptiveControl(int)));
  layout->addRow(new QLabel("Adaptive Control: "), chb);
  // bConstantRate
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bConstantRate ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbConstantRate(int)));
//  layout->addRow(new QLabel("bConstantrate: "), chb);
  // bCycleCount
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bCycleCount ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbCycleCount(int)));
//  layout->addRow(new QLabel("bCycleCount: "), chb);
  // bIncBlockCount
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bIncBlockCount ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbIncBlockCount(int)));
//  layout->addRow(new QLabel("bIncBlockCount: "), chb);
}

void MtlCycleTask::run()
{
  Task::setMtlTaskState(Task::kBusy);
  // расчет необходимого кол-ва квадрантов
  unsigned int qcycles = uQCycles*4 + (uEndQuadrant + 4 - uQuadrant)%4;
  // Запустить выполнение задачи машиной
  MTL32::GDSCycleBlockControlSegment(uCtrlCh,
                                     iMode,
                                     (float) fMean,
                                     (float) fAmplitude,
                                     (float) fFrequency,
                                     qcycles,
                                     (unsigned) uQuadrant,
                                     (unsigned) uWaveform,
                                     (BOOL) bRelative,
                                     (BOOL) bAdaptiveControl,
                                     (BOOL) bConstantRate,
                                     (BOOL) bIncCycleCount,
                                     (BOOL) bIncBlockCount);
  // Ожидание переключения режима
  msleep(1000);
  // Удерживать поток пока машина выполняет задачу
  while (getMachineState() == kBusy)
  {
    msleep(1000);
  }
  Task::setMtlTaskState(Task::kIdle);
}

void MtlCycleTask::write(QJsonObject & json)
{
  json["tasktype"] = QJsonValue("MTLCycle");

  json["Ctrl_Ch"] = QJsonValue(uCtrlCh);
  json["ControlMode"] = QJsonValue(iMode);
  json["Mean"] = QJsonValue(fMean);
  json["Amplitude"] = QJsonValue(fAmplitude);
  json["Frequency"] = QJsonValue(fFrequency);
  json["StartQuadrant"] = QJsonValue((int)uQuadrant);
  json["StopQuadrant"] = QJsonValue((int)uEndQuadrant);
  json["Waveform"] = QJsonValue((int)uWaveform);
  json["Cycles"] = QJsonValue((int)uQCycles);
  json["AdaptiveControl"] = QJsonValue(bAdaptiveControl);
  json["MinMax"] = QJsonValue(bMinMax);
}

void MtlCycleTask::read(const QJsonObject & json)
{
  uCtrlCh = json["Ctrl_Ch"].toInt();
  iMode = json["ControlMode"].toInt();
  fMean = (float)json["Mean"].toDouble();
  fAmplitude = (float)json["Amplitude"].toDouble();
  fFrequency = (float)json["Frequency"].toDouble();
  uQuadrant = (unsigned)json["StartQuadrant"].toInt();
  uEndQuadrant = (unsigned)json["StopQuadrant"].toInt();
  uWaveform = (unsigned)json["Waveform"].toInt();
  uQCycles = (unsigned)json["Cycles"].toInt();
  bAdaptiveControl = json["AdaptiveControl"].toInt();
  bMinMax = json["MinMax"].toBool();
}


void MtlCycleTask::setuCtrlCh(int val)
{
  uCtrlCh = val;
}

void MtlCycleTask::setiMode(int val)
{
  iMode = val;  
  if(val == 0 || val == 2)
  {
    qdsbMean->setSuffix(" mm");
    qdsbAmplitude->setSuffix(" mm");
    qdsbMin->setSuffix(" mm");
    qdsbMax->setSuffix(" mm");
  }
  else if(val == 1)
  {
    qdsbMean->setSuffix(" kN");
    qdsbAmplitude->setSuffix(" kN");
    qdsbMin->setSuffix(" kN");
    qdsbMax->setSuffix(" kN");
  }
}

void MtlCycleTask::setfMean(double val)
{
  fMean = val;

  qdsbMin->blockSignals(true);
  qdsbMax->blockSignals(true);

  qdsbMin->setValue(fMean-fAmplitude);
  qdsbMax->setValue(fMean+fAmplitude);

  qdsbMin->blockSignals(false);
  qdsbMax->blockSignals(false);
}

void MtlCycleTask::setfAmplitude(double val)
{
  fAmplitude = val;

  qdsbMin->blockSignals(true);
  qdsbMax->blockSignals(true);

  qdsbMin->setValue(fMean-fAmplitude);
  qdsbMax->setValue(fMean+fAmplitude);

  qdsbMin->blockSignals(false);
  qdsbMax->blockSignals(false);
}

void MtlCycleTask::setfFrequency(double val)
{
  fFrequency = val;
}

void MtlCycleTask::setuQCycles(int val)
{
  if (val == 0)
    QMessageBox::information(QApplication::topLevelWidgets().first(),
                             "Pylon Info",
                             "Zero cycles",
                             QMessageBox::Ok);
  uQCycles = val;
}

void MtlCycleTask::setuQuadrant(int val)
{
  uQuadrant = val;
}

void MtlCycleTask::setuEndQuadrant(int val)
{
  uEndQuadrant = val;
}

void MtlCycleTask::setuWaveform(int val)
{
  uWaveform = val;
}

void MtlCycleTask::setbRelative(int val)
{
  bRelative = val;
}

void MtlCycleTask::setbAdaptiveControl(int val)
{
  bAdaptiveControl = val;
}

void MtlCycleTask::setbConstantRate(int val)
{
  bConstantRate = val;
}

void MtlCycleTask::setbCycleCount(int val)
{
  bIncCycleCount = val;
}

void MtlCycleTask::setbIncBlockCount(int val)
{
  bIncBlockCount = val;
}

void MtlCycleTask::setMin(double val)
{
  qdsbMean->blockSignals(true);
  qdsbAmplitude->blockSignals(true);

  double maxVal = qdsbMax->value();

  fAmplitude =  (maxVal - val)/2;
  fMean = maxVal - fAmplitude;

  qdsbMean->setValue(fMean);
  qdsbAmplitude->setValue(fAmplitude);

  qdsbMean->blockSignals(false);
  qdsbAmplitude->blockSignals(false);
}

void MtlCycleTask::setMax(double val)
{
  qdsbMean->blockSignals(true);
  qdsbAmplitude->blockSignals(true);

  double minVal = qdsbMin->value();

  fAmplitude =  (val - minVal)/2;
  fMean = val - fAmplitude;

  qdsbMean->setValue(fMean);
  qdsbAmplitude->setValue(fAmplitude);

  qdsbMean->blockSignals(false);
  qdsbAmplitude->blockSignals(false);
}

void MtlCycleTask::setbMinMax(bool val)
{
  bMinMax = val;
}
