// Include files to use the PYLON API.

#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
using namespace Pylon;
using namespace Basler_GigECameraParams;

#include <QApplication>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QFile>
#include <QMessageBox>
#include <QSharedPointer>
#include "basler.h"


void ReconnectThread::run()
{
  while (true)
  {
    //BaslerTask::retriveCamList();

    //{
    //  QWriteLocker write_locker(&BaslerTask::cam_map_lock);
    //  //
    //  for (int i = 0; i < BaslerTask::cam_map.count(); ++i)
    //  {
    //    QString cam_name = (BaslerTask::cam_map.begin()+i).key();
    //    if (!BaslerTask::cam_map[cam_name] ||
    //        BaslerCap_IsCameraDeviceRemoved(BaslerTask::cam_map[cam_name]))
    //    {
    //      // delete _cam_name object
    //      if (!cam_name.isEmpty() && BaslerTask::cam_map[cam_name])
    //      {
    //        BaslerCap_delete(BaslerTask::cam_map[cam_name]);
    //        BaslerTask::cam_map[cam_name] = 0;
    //      }

    //      // reconnect
    //      //if (!cam_name.isEmpty() &&
    //      //    BaslerTask::cam_id_map.find(cam_name) != BaslerTask::cam_id_map.end())
    //        //BaslerTask::cam_map[cam_name] = BaslerCap_create((BaslerCamId) BaslerTask::cam_id_map[cam_name]); !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //    }
    //  }
    //}
    //// timeout
    //msleep(5000);
  }
}

QStringList BaslerTask::FileFormatExt = (QStringList() <<
                                         "bmp" <<
                                         "tiff" <<
                                         "jpeg" <<
                                         "png");

ReconnectThread            BaslerTask::reconnect_thread;

BaslerTask::BaslerTask()
  : timer(this),
    basler_camera(this),
    isTestShoot(false)
{
  has_settings     = 0;
  settings_file    = "";
  settings_file_le = 0;
  multiple_shot    = 0;
  timeout          = 1000;
  vars_cb          = 0;
  setfps           = false;
  fps              = 5;
  file_format      = 0;
  multiple_shoot_count = 0;

  connect(&basler_camera, &BaslerCamera::exceptionOccurred, this, &BaslerTask::exceptionMessage);
  connect(&timer, &QTimer::timeout, &basler_camera, &BaslerCamera::executeTrigger);
  connect(this, SIGNAL(beginThread()), &timer, SLOT(start()), Qt::BlockingQueuedConnection);
  connect(this, SIGNAL(endThread()), &timer, SLOT(stop()), Qt::BlockingQueuedConnection);

  // fill vars list
  vars["CamName"]  = QVariant(QVariant::Type::String);
  vars["FrameCount"]  = QVariant(0);

  filename_template = "{CamName}";
  splitTemplateName(filename_template, var_list);

  static int cam_num = 0;
  vars["CamName"].setValue(QString("Basler-%1").arg(cam_num));
  cam_num++;
}

BaslerTask::~BaslerTask()
{
}

void BaslerTask::createForm(QFormLayout *layout)
{
  // ----- cam list combobox
  // retrive camera list
//  retriveCamList();
//  cam_map_lock.lockForRead();
  QStringList cam_list = BaslerCamera::getCamNames();
  // add controls to form
  cam_cb = new QComboBox(layout->parentWidget());
  cam_cb->addItems(cam_list);
//  cam_map_lock.unlock();
  cam_cb->setCurrentIndex(-1);
  connect(cam_cb, SIGNAL(currentIndexChanged(const QString &)), SLOT(setCamIndex(const QString &)));
  connect(cam_cb, SIGNAL(currentIndexChanged(const QString &)), &basler_camera, SLOT(setCamera(const QString &)));

  if (cam_name.isEmpty())
  {
    if (!cam_list.isEmpty())
    {
      cam_name = cam_list.first();
      cam_cb->setCurrentIndex(0);
    }
  }
  else
  {
    cam_cb->setCurrentText(cam_name);
  }

  layout->addRow(new QLabel("Camera: "), cam_cb);
  // ---- var cam_name
  QLineEdit * le = new QLineEdit(layout->parentWidget());
  le->setText(vars["CamName"].toString());
  connect(le, SIGNAL(textChanged(QString)), SLOT(setVarCamName(QString)));
  layout->addRow(new QLabel("Camera Name: "), le);
  // ---- cam_settings
  QCheckBox * chb = new QCheckBox(layout->parentWidget());
  chb->setCheckState(has_settings ? Qt::Checked : Qt::Unchecked);
  connect(chb, SIGNAL(stateChanged(int)), SLOT(setCamSettings(int)));
  layout->addRow(new QLabel("Load Settings: "), chb);
  // ---- settings_file
  settings_file_le = new LineFileBrowse(layout->parentWidget());
  settings_file_le->setText(settings_file);
  connect(settings_file_le, SIGNAL(editingFinished()), SLOT(setSettingsFile()));
  connect(chb, SIGNAL(toggled(bool)), settings_file_le,  SLOT(setEnabled(bool)));
  settings_file_le->setEnabled(has_settings ? Qt::Checked : Qt::Unchecked);
  layout->addRow(new QLabel("Settings File: "), settings_file_le);

  // ---- output directory
  outdir_le = new LineFileBrowse(layout->parentWidget(), true);
  outdir_le->setText(out_dir);
  connect(outdir_le, SIGNAL(editingFinished()), SLOT(setOutDir()));
  layout->addRow(new QLabel("Output Directory: "), outdir_le);

  // ----- vars for template
  vars_cb = new QComboBox(layout->parentWidget());
  vars_cb->addItems(global_var_names);
  vars_cb->addItems(vars.keys());
  QPushButton *add_var_btn = new QPushButton("Add Var");
  connect(add_var_btn, SIGNAL(clicked(bool)), SLOT(addVarToTemplate()));
  layout->addRow(add_var_btn, vars_cb);
  // ---- filename_template
  filename_template_le = new QLineEdit(layout->parentWidget());
  filename_template_le->setText(filename_template);
  connect(filename_template_le, SIGNAL(textChanged(QString)), SLOT(setFilenameTemplate(QString)));
  layout->addRow(new QLabel("Output Filename: "), filename_template_le);

  // ----- multiple shot
  QCheckBox * mshot_chb = new QCheckBox(layout->parentWidget());
  connect(mshot_chb, SIGNAL(stateChanged(int)), SLOT(setMultipleShot(int)));
  layout->addRow(new QLabel("Multiple Shot: "), mshot_chb);
  // ----- fps
  fps_chb = new QCheckBox(layout->parentWidget());
  fps_chb->setText("FPS: ");
  fps_chb->setAutoExclusive(true);
  connect(fps_chb, SIGNAL(toggled(bool)), SLOT(toggleFPS(bool)));
  fps_sb = new QDoubleSpinBox(layout->parentWidget());
  fps_sb->setRange(0.0, 1000.0);
  fps_sb->setValue(fps);
  connect(fps_sb, SIGNAL(valueChanged(double)), SLOT(setFPS(double)));
  layout->addRow(fps_chb, fps_sb);
  connect(fps_chb, SIGNAL(toggled(bool)), fps_sb, SLOT(setEnabled(bool)));
  // ----- timeout
  timeout_chb = new QCheckBox(layout->parentWidget());
  timeout_chb->setText("Timeout: ");
  timeout_chb->setAutoExclusive(true);
  timeout_sb = new QSpinBox(layout->parentWidget());
  timeout_sb->setRange(0, INT_MAX);
  timeout_sb->setValue(timeout);
  timeout_sb->setSuffix(" ms");
  timeout_sb->setEnabled(multiple_shot);
  connect(timeout_sb, SIGNAL(valueChanged(int)), SLOT(setTimeout(int)));
  layout->addRow(timeout_chb, timeout_sb);
  connect(timeout_chb, SIGNAL(toggled(bool)), timeout_sb, SLOT(setEnabled(bool)));

  fps_chb->setCheckState(setfps ? Qt::Checked : Qt::Unchecked);
  timeout_chb->setCheckState(setfps ? Qt::Unchecked: Qt::Checked);
  // fps & timeout initial state is disable
  fps_sb->setDisabled(true);
  fps_chb->setDisabled(true);
  timeout_sb->setDisabled(true);
  timeout_chb->setDisabled(true);
  //
  mshot_chb->setCheckState(multiple_shot ? Qt::Checked : Qt::Unchecked);
  // ----- file format
  QComboBox * ff_cb = new QComboBox(layout->parentWidget());
  ff_cb->addItems(FileFormatExt);
  ff_cb->setCurrentIndex(file_format);
  connect(ff_cb, SIGNAL(currentIndexChanged(int)), SLOT(setFileFormat(int)));
  layout->addRow(new QLabel("File Format: "), ff_cb);
  // ----- frame counter
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setRange(0, INT_MAX);
  sb->setValue(vars["FrameCount"].toInt());
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setFrameCount(int)));
  layout->addRow(new QLabel("Frame Count: "), sb);
  // ----- test shoot button
  QPushButton * test_btn = new QPushButton("Test Shot", layout->parentWidget());
  connect(test_btn, SIGNAL(clicked(bool)), SLOT(testShoot()));
  layout->addRow(new QLabel(""), test_btn);
}

void BaslerTask::write(QJsonObject & json)
{
  json["tasktype"]          = QJsonValue("Basler");

  json["cam_name"]          = QJsonValue(cam_name);
  json["var_cam_name"]      = QJsonValue(vars["CamName"].toString());
  json["out_dir"]           = QJsonValue(out_dir);
  json["has_settings"]      = QJsonValue(has_settings);
  json["settings_file"]     = QJsonValue(settings_file);
  json["filename_template"] = QJsonValue(filename_template);
  json["multiple_shot"]     = QJsonValue(multiple_shot);
  json["setfps"]            = QJsonValue(setfps);
  json["fps"]               = QJsonValue(fps);
  json["timeout"]           = QJsonValue(timeout);
  json["file_format"]       = QJsonValue(file_format);
}

void BaslerTask::read(const QJsonObject & json)
{
  cam_name        = json["cam_name"].toString();
  vars["CamName"] = json["var_cam_name"].toString();
  out_dir         = json["out_dir"].toString();
  has_settings    = json["has_settings"].toInt();
  settings_file   = json["settings_file"].toString();
  multiple_shot   = json["multiple_shot"].toInt();
  setfps          = json["setfps"].toBool();
  fps             = json["fps"].toDouble();
  timeout         = json["timeout"].toInt();
  file_format     = json["file_format"].toInt();

  setFilenameTemplate(json["filename_template"].toString());

  loadSettings();

  if (multiple_shot && setfps)
  {
    basler_camera.open();
    basler_camera.setFPS(fps);
    basler_camera.close();
  }
}

void BaslerTask::initialize()
{
}

void BaslerTask::finalize()
{
}

void BaslerTask::OnImageGrabbed(CInstantCamera& _camera, const CGrabResultPtr& ptrGrabResult)
{
  grab_result.wakeAll();

  // generated file name in accordance with the template
  QVariantList _var_list = var_list;
  {
    QReadLocker lock(&global_vars_lock);
    substVars(_var_list, global_vars);
  }
  substVars(_var_list, vars);
  QString output_filename = composeNameFromList(_var_list);

  if (isTestShoot)
  {
    isTestShoot = false;

    output_filename = out_dir + "/" + output_filename + "-test." + FileFormatExt[file_format];
  }
  else
  {
    output_filename = out_dir + "/" + output_filename;

    if (multiple_shot)
      output_filename += QString("-%1").arg(multiple_shoot_count++);

    output_filename += "." + FileFormatExt[file_format];

    vars["FrameCount"].setValue(vars["FrameCount"].toInt() + 1);
  }

  if (ptrGrabResult.IsValid() && ptrGrabResult->GrabSucceeded())
    CImagePersistence::Save((EImageFileFormat) file_format, output_filename.toLocal8Bit().constData(), ptrGrabResult);
}

void BaslerTask::run()
{
  basler_camera.open();
  basler_camera.startGrabbing();
  if (multiple_shot)
  {
    multiple_shoot_count = 0;
    if (setfps)
      do {} while (getMtlTaskState() == kBusy);
    else
      do
      {
        basler_camera.executeTrigger();
        // timeout
        msleep(timeout);
      } while (getMtlTaskState() == kBusy);
    //
  }
  else
  {
    frame_mutex.lock();
    basler_camera.executeTrigger();
    grab_result.wait(&frame_mutex, 5000);
    frame_mutex.unlock();
  }

  basler_camera.stopGrabbing();
  basler_camera.close();
}

void BaslerTask::setCamIndex(const QString & val)
{
  cam_name = val;
  loadSettings();
}

void BaslerTask::setVarCamName(const QString & val)
{
  vars["CamName"].setValue(val);
}

void BaslerTask::setCamSettings(int val)
{
  has_settings = val;
}

void BaslerTask::setSettingsFile()
{
  if (settings_file_le)
    settings_file = settings_file_le->text();
  loadSettings();
}

void BaslerTask::setOutDir()
{
  if (outdir_le)
    out_dir = outdir_le->text();
}

void BaslerTask::setFilenameTemplate(const QString & val)
{
  filename_template = val;

  splitTemplateName(filename_template, var_list);
}

void BaslerTask::setMultipleShot(int val)
{
  multiple_shot = val;

  if (multiple_shot)
  {
    fps_sb->setEnabled(setfps);
    fps_chb->setEnabled(true);
    timeout_sb->setEnabled(!setfps);
    timeout_chb->setEnabled(true);

    if (setfps)
    {
      basler_camera.open();
      basler_camera.setFPS(fps);
      basler_camera.close();
    }
  }
  else
  {
    fps_sb->setDisabled(true);
    fps_chb->setDisabled(true);
    timeout_sb->setDisabled(true);
    timeout_chb->setDisabled(true);
  }
}

void BaslerTask::setTimeout(int val)
{
  timeout = val;
}

void BaslerTask::addVarToTemplate()
{
  QString var_name = vars_cb->currentText();
  filename_template_le->insert("{" + var_name + "}");
}

void BaslerTask::testShoot()
{
  isTestShoot = true;

  basler_camera.open();

  frame_mutex.lock();
  if (multiple_shot && setfps)
  {
    basler_camera.startGrabbing(1);
  }
  else
  {
    basler_camera.startGrabbing();
    basler_camera.executeTrigger();
  }
  grab_result.wait(&frame_mutex, 5000);
  frame_mutex.unlock();

  basler_camera.stopGrabbing();
  basler_camera.close();
}

void BaslerTask::setFileFormat(int val)
{
  file_format = val;
}

void BaslerTask::setFrameCount(int val)
{
  vars["FrameCount"].setValue(QVariant(val));
}

void BaslerTask::setFPS(double val)
{
  fps = val;
  basler_camera.open();
  basler_camera.setFPS(fps);
  basler_camera.close();
}

void BaslerTask::toggleFPS(bool val)
{
  setfps = val;
  if (val) 
    basler_camera.setMode(kBaslerContinuous);
  else
    basler_camera.setMode(kBaslerSoftwareTrigger);

  if (multiple_shot && setfps)
  {
    basler_camera.open();
    basler_camera.setFPS(fps);
    basler_camera.close();
  }
}

void BaslerTask::exceptionMessage(const QString & msg)
{
  QMessageBox::critical(QApplication::topLevelWidgets().first(),
                        "Pylon Error",
                        msg,
                        QMessageBox::Ok);
}

void BaslerTask::loadSettings()
{
  if (!settings_file.isEmpty() && has_settings)
  {
    basler_camera.loadSettings(settings_file);
  }
}

void BaslerTask::test()
{
  //int countOfImagesToGrab = 100;
  //SYSTEMTIME st1, st2;
  //try
  //{
  //  // Create an instant camera object with the camera device found first.

  //  if (camera.isNull()) return;
  //  // The parameter MaxNumBuffer can be used to control the count of buffers
  //  // allocated for grabbing. The default value of this parameter is 10.


  //  camera->MaxNumBuffer = 5;

  //  // Start the grabbing of c_countOfImagesToGrab images.
  //  // The camera device is parameterized with a default configuration which
  //  // sets up free-running continuous acquisition.
  //  GetSystemTime(&st1);
  //  camera->StartGrabbing(countOfImagesToGrab);

  //  // This smart pointer will receive the grab result data.
  //  CGrabResultPtr ptrGrabResult;

  //  // Camera.StopGrabbing() is called automatically by the RetrieveResult() method
  //  // when c_countOfImagesToGrab images have been retrieved.
  //  while (camera->IsGrabbing())
  //  {
  //    // Wait for an image and then retrieve it. A timeout of 5000 ms is used.
  //    camera->RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);
  //    // Image grabbed successfully?
  //    if (ptrGrabResult->GrabSucceeded())
  //    {
  //      // Access the image data.
  //      //cout << "SizeX: " << ptrGrabResult->GetWidth() << endl;
  //      //cout << "SizeY: " << ptrGrabResult->GetHeight() << endl;
  //      const uint8_t *pImageBuffer = (uint8_t *)ptrGrabResult->GetBuffer();
  //      //cout << "Gray value of first pixel: " << (uint32_t)pImageBuffer[0] << endl << endl;
  //    }
  //    else
  //    {
  //      //cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
  //    }
  //  }
  //}
  //catch (const GenericException &e)
  //{
  //  // Error handling.
  //  QMessageBox::critical(QApplication::topLevelWidgets().first(),
  //                        "Pylon Error",
  //                        QString("An exception occurred.\n%1").arg(e.GetDescription()),
  //                        QMessageBox::Ok);
  //}

  //GetSystemTime(&st2);
  //double diff =
  //  (st2.wHour - st1.wHour) * 60.0 * 60.0 * 1000.0 +
  //  (st2.wMinute - st1.wMinute) * 60.0 * 1000.0 +
  //  (st2.wSecond - st1.wSecond) * 1000.0 +
  //  (st2.wMilliseconds - st1.wMilliseconds);
  //QMessageBox::information(QApplication::topLevelWidgets().first(),
  //                         "Pylon Info",
  //                         QString("Time: %1\n FPS: %2").arg(diff).arg((double)1000.0*countOfImagesToGrab/diff),
  //                         QMessageBox::Ok);
}