#ifndef TASK_H
#define TASK_H

#include <QList>
#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QFormLayout>
#include <QThread>
#include <QReadWriteLock>
#include <QJsonObject>

// class Task
// base class for task objects
class Task : public QThread
{
    Q_OBJECT
  public:
    enum MachineState { kBusy = 0, kIdle = 1 };
  public:
    Task();
    // class fabric
    // фабрика классов
    // при добавлении нового типа задачи необходимо
    // добавить вызов соответствующего конструктора в данную функцию
    static Task* create(const QString & task_type);
    // returns task list
    // возвращает список имен задач
    // при добавлении нового типа задачи необходимо
    // добавить имя задачи в данную функцию
    static QStringList listTaskName();
    // create gui form
    // создание графической формы
    // функция должна быть реализована для класса задачи
    // форма служит для отображения/изменения свойств задачи
    virtual void createForm(QFormLayout *layout);

    // sets global variable by index
    static void setGlobalVar(const QString & index, QVariant val);
    // returns global variable by index
    static const QVariant getGlobalVar(const QString & index);
    // returns list of MTL variables
    static const QStringList getGlobalVarNames();
    // returns machine state
    static const int getMachineState();
    // returns state of MtlTask instances (kBusy | kIdle)
    static const int getMtlTaskState();
    static const void setMtlTaskState(MachineState state);
    // fill out the list of global variables
    static QStringList initGlobalVars();
    // split template string of output filename
    static void splitTemplateName(const QString & templ, QVariantList & templ_items);
    static void substVars(QVariantList & templ_items, QMap<QString,QVariant> &vars);
    static QString composeNameFromList(QVariantList & templ_items);
    // write/read task properties
    virtual void write(QJsonObject & json);
    virtual void read(const QJsonObject & json);
    // initialize / finalize task
    // use for open/close camera
    virtual void initialize();
    virtual void finalize();
  protected:
    // execute task
    virtual void run();
  protected:
    static QMap<QString,QVariant> global_vars;
    static QReadWriteLock global_vars_lock;
    static QStringList    global_var_names;
    // state of test machine ! 1 - idle | 0 - busy
    static int            machine_state;
    static QReadWriteLock machine_state_lock;
    static int            mtltask_state;
    static QReadWriteLock mtltask_state_lock;
};

// task model
// tree model with 2 levels
class QTasksModel : public QStandardItemModel
{
   Q_OBJECT
  public:
    ~QTasksModel();
    enum { TreeLevelRole = Qt::UserRole + 1, ItemDataRole };

    void addBlock();
    void remove(const QModelIndexList & index_list);
    void addTask(const QModelIndex &index, const QString &task_type);
    void save(const QString & filename);
    void load(const QString & filename);

    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const;
  protected:
    void writeRoot(QJsonObject & json);
    void writeItem(QStandardItem &item, QJsonObject & json);
    void readRoot(const QJsonObject &json);
    void readItem(const QJsonObject & json, QStandardItem &item);

  public slots:
    void setNBlocks(int nb);
  signals:
    void nblocksChanged(int);
  private:
    int nblocks = 1;
};

#endif // TASK_H
