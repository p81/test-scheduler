#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include "task_template.h"

TemplateTask::TemplateTask()
{
  int_property = 0;
  dbl_property = 0.0;
}

void TemplateTask::createForm(QFormLayout *layout)
{
  // integer property control
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setValue(int_property);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setIntProperty(int)));
  layout->addRow(new QLabel("IntProperty: "), sb);

  // double property
  QDoubleSpinBox * dblsb = new QDoubleSpinBox(layout->parentWidget());
  dblsb->setValue(dbl_property);
  connect(dblsb, SIGNAL(valueChanged(double)), SLOT(setDblProperty(double)));
  layout->addRow(new QLabel("DblProperty: "), dblsb);
}

void TemplateTask::run()
{
}

void TemplateTask::write(QJsonObject &json)
{
  json["tasktype"] = QJsonValue("Template");

  json["int_property"] = QJsonValue(int_property);
  json["dbl_property"] = QJsonValue(dbl_property);
}

void TemplateTask::read(const QJsonObject & json)
{
  int_property = json["int_property"].toInt();
  dbl_property = json["dbl_property"].toDouble();
}

void TemplateTask::setIntProperty(int val)
{
  int_property = val;
}

void TemplateTask::setDblProperty(double val)
{
  dbl_property = val;
}
