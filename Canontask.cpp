#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include "Canontask.h"

#include <cstdio>
#include <iostream>
using namespace std;

QStringList CanonTask::FileFormatExt = (QStringList() << "JPEG");

CanonTask::CanonTask()
{
  has_settings     = 0;
  settings_file    = "";
  le_settings_file  = 0;
  multiple_shoot    = 0;
  timeout         = 1000;
  cb_vars          = 0;
  image_quality    = 100;
  image_format     = FileFormatExt[0];

  // fill vars list
  vars["CamName"]  = QVariant(QVariant::Type::String);
  vars["FrameCount"]  = QVariant(0);

  filename_template = "{CamName}";
  splitTemplateName(filename_template, var_list);

  static int cam_num = 0;
  vars["CamName"].setValue(QString("Canon-%1").arg(cam_num));
  cam_num++;

  is_test_shoot = false;

  connect(this, &CanonTask::softwareShoot, &canon_eos, &CanonEOS::takePhoto);
  connect(&canon_eos, &CanonEOS::downloadCompleted, this, &CanonTask::takePhotoComplete);
}

CanonTask::~CanonTask()
{
}

void CanonTask::createForm(QFormLayout *layout)
{
  // ----- cam list combobox
  // retrive camera list
  cams_map = CanonEOS::getCamNames();
  // add controls to form
  cb_cam = new QComboBox(layout->parentWidget());
  cb_cam->addItems(cams_map.values());
  connect(cb_cam, SIGNAL(currentIndexChanged(int)), SLOT(setCamIndex(int)));
  connect(this, &CanonTask::cameraSelected, &canon_eos, &CanonEOS::registerCamera);

  if (cam_id.isEmpty())
  {
    if (!cams_map.isEmpty())
    {
      cam_id = cams_map.firstKey();
      cb_cam->setCurrentIndex(0);
      // select camera
      emit cameraSelected(cam_id);
    }
  }
  else
  {
    QMap<QString, QString>::iterator cam_id_it = cams_map.find(cam_id);
    if (cam_id_it != cams_map.end())
    {
      int index = std::distance(cams_map.begin(), cam_id_it);
      cb_cam->setCurrentIndex(index);
    }
    else
    {
      if (!cams_map.isEmpty())
        cb_cam->setCurrentIndex(-1);
    }
  }

  layout->addRow(new QLabel("Camera: "), cb_cam);
  // ---- var cam_name
  QLineEdit * le = new QLineEdit(layout->parentWidget());
  le->setText(vars["CamName"].toString());
  connect(le, SIGNAL(textChanged(QString)), SLOT(setVarCamName(QString)));
  layout->addRow(new QLabel("Camera Name: "), le);
  // ---- cam_settings
  //QCheckBox * chb = new QCheckBox(layout->parentWidget());
  //chb->setCheckState(has_settings ? Qt::Checked : Qt::Unchecked);
  //connect(chb, SIGNAL(stateChanged(int)), SLOT(setCamSettings(int)));
  //layout->addRow(new QLabel("Load Settings: "), chb);
  // ---- settings_file
  //le_settings_file = new LineFileBrowse(layout->parentWidget());
  //le_settings_file->setText(settings_file);
  //connect(le_settings_file, SIGNAL(editingFinished()), SLOT(setSettingsFile()));
  //connect(chb, SIGNAL(toggled(bool)), le_settings_file,  SLOT(setEnabled(bool)));
  //le_settings_file->setEnabled(has_settings ? Qt::Checked : Qt::Unchecked);
  //layout->addRow(new QLabel("Settings File: "), le_settings_file);

  // ---- output directory
  le_out_dir = new LineFileBrowse(layout->parentWidget(), true);
  le_out_dir->setText(out_dir);
  connect(le_out_dir, SIGNAL(editingFinished()), SLOT(setOutDir()));
  layout->addRow(new QLabel("Output Directory: "), le_out_dir);

  // ----- vars for template
  cb_vars = new QComboBox(layout->parentWidget());
  cb_vars->addItems(global_var_names);
  cb_vars->addItems(vars.keys());
  QPushButton *add_var_btn = new QPushButton("Add Var");
  connect(add_var_btn, SIGNAL(clicked(bool)), SLOT(addVarToTemplate()));
  layout->addRow(add_var_btn, cb_vars);
  // ---- filename_template
  le_filename_template = new QLineEdit(layout->parentWidget());
  le_filename_template->setText(filename_template);
  connect(le_filename_template, SIGNAL(textChanged(QString)), SLOT(setFilenameTemplate(QString)));
  layout->addRow(new QLabel("Output Filename: "), le_filename_template);
  // ----- multiple shot
  QCheckBox * chbMultiple = new QCheckBox(layout->parentWidget());
  chbMultiple->setCheckState(multiple_shoot ? Qt::Checked : Qt::Unchecked);
  connect(chbMultiple, SIGNAL(stateChanged(int)), SLOT(setMultipleShot(int)));
  layout->addRow(new QLabel("Multiple Shot: "), chbMultiple);
  // ----- timeout
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setRange(0, INT_MAX);
  sb->setValue(timeout);
  sb->setSuffix(" ms");
  sb->setEnabled(multiple_shoot);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setTimeout(int)));
  layout->addRow(new QLabel("Timeout: "), sb);
  connect(chbMultiple, SIGNAL(toggled(bool)), sb, SLOT(setEnabled(bool)));
  // ----- frame counter
  sb = new QSpinBox(layout->parentWidget());
  sb->setRange(0, INT_MAX);
  sb->setValue(vars["FrameCount"].toInt());
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setFrameCount(int)));
  layout->addRow(new QLabel("Frame Count: "), sb);
  // ----- image format
  QComboBox * ff_cb = new QComboBox(layout->parentWidget());
  ff_cb->addItems(FileFormatExt);
  ff_cb->setCurrentText(image_format);
  connect(ff_cb, SIGNAL(currentIndexChanged(QString)), SLOT(setImageFormat(QString)));
  layout->addRow(new QLabel("Image Format: "), ff_cb);
  // ----- image quality
  //sb = new QSpinBox(layout->parentWidget());
  //sb->setRange(0, 100);
  //sb->setValue(image_quality);
  //connect(sb, SIGNAL(valueChanged(int)), SLOT(setImageQuality(int)));
  //layout->addRow(new QLabel("Image Quality: "), sb);
  // ----- test shoot button
  QPushButton * test_btn = new QPushButton("Test Shot", layout->parentWidget());
  connect(test_btn, SIGNAL(clicked(bool)), SLOT(testShoot()));
  layout->addRow(new QLabel(""), test_btn);
}

void CanonTask::write(QJsonObject & json)
{
  json["tasktype"]          = QJsonValue("Canon");
  json["cam_name"]          = QJsonValue(cam_name);
  json["var_cam_name"]      = QJsonValue(vars["CamName"].toString());
  json["out_dir"]           = QJsonValue(out_dir);
  json["has_settings"]      = QJsonValue(has_settings);
  json["settings_file"]     = QJsonValue(settings_file);
  json["filename_template"] = QJsonValue(filename_template);
  json["multiple_shot"]     = QJsonValue(multiple_shoot);
  json["timeout"]           = QJsonValue(timeout);
  json["image_format"]      = QJsonValue(image_format);
  json["image_quality"]     = QJsonValue(image_quality);
}

void CanonTask::read(const QJsonObject & json)
{
  cam_name         = json["cam_name"].toString();
  vars["CamName"] = json["var_cam_name"].toString();
  out_dir          = json["out_dir"].toString();
  has_settings     = json["has_settings"].toInt();
  settings_file    = json["settings_file"].toString();
  multiple_shoot    = json["multiple_shot"].toInt();
  timeout         = json["timeout"].toInt();
  image_format     = json["image_format"].toString();
  image_quality    = json["image_quality"].toInt();

  setFilenameTemplate(json["filename_template"].toString());
}

void CanonTask::initialize()
{
}

void CanonTask::finalize()
{
}

void CanonTask::run()
{
  if (multiple_shoot)
  {
    multiple_shoot_count = 0;
    do
    {
      frame_mutex.lock();
      emit softwareShoot();
      // timeout
      msleep(timeout);
      grab_result.wait(&frame_mutex, 5000);
      frame_mutex.unlock();
    }
    while (getMtlTaskState() == kBusy);
  }
  else
  {
    frame_mutex.lock();
    emit softwareShoot();
    grab_result.wait(&frame_mutex, 5000);
    frame_mutex.unlock();
  }
}

void CanonTask::setCamIndex(int val)
{
  if (val < 0) return;

  QMap<QString, QString>::iterator cam_id_it = cams_map.begin() + val;
  if (cam_id_it != cams_map.end())
  {
    cam_id = cam_id_it.key();
    emit cameraSelected(cam_id);
  }
}

void CanonTask::setVarCamName(const QString & val)
{
  vars["CamName"].setValue(val);
}

void CanonTask::setCamSettings(int val)
{
  has_settings = val;
}

void CanonTask::setSettingsFile()
{
  if (le_settings_file)
    settings_file = le_settings_file->text();
//  if (!settings_file.isEmpty() && has_settings)
//    CanonCap_loadSettings(cam_map[cam_name], settings_file.toLocal8Bit().constData());
}

void CanonTask::setOutDir()
{
  if (le_out_dir)
    out_dir = le_out_dir->text();
}

void CanonTask::setFilenameTemplate(const QString & val)
{
  filename_template = val;

  splitTemplateName(filename_template, var_list);
}

void CanonTask::setMultipleShot(int val)
{
  multiple_shoot = val;
}

void CanonTask::setTimeout(int val)
{
  timeout = val;
}

void CanonTask::addVarToTemplate()
{
  QString var_name = cb_vars->currentText();
  le_filename_template->insert("{" + var_name + "}");
}

void CanonTask::testShoot()
{
  is_test_shoot = true;

  frame_mutex.lock();
  //  emit softwareoneframe(cam_id);
  emit softwareShoot();

  grab_result.wait(&frame_mutex, 5000);
  frame_mutex.unlock();
}

void CanonTask::setFrameCount(int val)
{
    vars["FrameCount"].setValue(QVariant(val));
}

void CanonTask::setImageQuality(int val)
{
  image_quality = val;
}

void CanonTask::setImageFormat(const QString & val)
{
  image_format = val;
}

void CanonTask::takePhotoComplete(const uchar * data, int size)
{
  grab_result.wakeAll();
  // generated file name in accordance with the template
  QVariantList _var_list = var_list;
  {
    QReadLocker lock(&global_vars_lock);
    substVars(_var_list, global_vars);
  }
  substVars(_var_list, vars);
  QString output_filename = composeNameFromList(_var_list);

  if(is_test_shoot)
  {
    is_test_shoot = false;

    output_filename = out_dir + "/" + output_filename + "-test." + image_format;

    FILE * image = fopen(output_filename.toStdString().c_str(), "wb");
    fwrite(data, size, 1, image);
    fclose(image);
  }
  else
  {
    if (multiple_shoot)
      output_filename = out_dir + "/" + output_filename + QString("-%1").arg(multiple_shoot_count++) + "." + image_format;
    else
      output_filename = out_dir + "/" + output_filename + "." + image_format;
    FILE * image = fopen(output_filename.toStdString().c_str(), "wb");
    fwrite(data, size, 1, image);
    fclose(image);

    vars["FrameCount"].setValue(vars["FrameCount"].toInt() + 1);
  }

//  OutputDebugString("complete-unlock\n");
}

