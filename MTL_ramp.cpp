#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QCheckBox>

#include "MTL_ramp.h"

MtlRampTask::MtlRampTask()
  : MtlTask()
{
  uCtrlCh = 0;
  iMode = 0;
  fToSetPoint = 0;
  fGain = 1;
  fFrequency = 1;
  uWaveform = 0;
  bRelative = 0;
  bConstantRate = 1;
}

void MtlRampTask::createForm(QFormLayout *layout)
{
  // uCtrlCh
  QSpinBox * sb = new QSpinBox(layout->parentWidget());
  sb->setValue(uCtrlCh);
  connect(sb, SIGNAL(valueChanged(int)), SLOT(setuCtrlCh(int)));
  layout->addRow(new QLabel("Actuator: "), sb);

  // iMode
  QComboBox * cb = new QComboBox(layout->parentWidget());
  cb->addItem("Stroke");
  cb->addItem("Load");
  cb->addItem("Strain");
  cb->setCurrentIndex(iMode);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setiMode(int)));
  layout->addRow(new QLabel("Control Mode: "), cb);

  // fToSetPoint
  dblsbSP = new QDoubleSpinBox(layout->parentWidget());
  dblsbSP->setRange(-500,500);
  dblsbSP->setValue(fToSetPoint);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    dblsbSP->setSuffix(" mm");
  }
  else if(cb->currentIndex() == 1)
  {
    dblsbSP->setSuffix(" kN");
  }
  connect(dblsbSP, SIGNAL(valueChanged(double)), SLOT(setfToSetPoint(double)));
  layout->addRow(new QLabel("Set Point: "), dblsbSP);

//  // fGain
//  /*QDoubleSpinBox * */dblsb = new QDoubleSpinBox(layout->parentWidget());
//  dblsb->setValue(fGain);
//  connect(dblsb, SIGNAL(valueChanged(double)), SLOT(setfGain(double)));
//  layout->addRow(new QLabel("Gain: "), dblsb);

  // fFrequency
  dblsbRate = new QDoubleSpinBox(layout->parentWidget());
  dblsbRate->setRange(-500,500);
  dblsbRate->setValue(fFrequency);
  if(cb->currentIndex() == 0 || cb->currentIndex() == 2)
  {
    dblsbRate->setSuffix(" mm/s");
  }
  else if(cb->currentIndex() == 1)
  {
    dblsbRate->setSuffix(" kN/s");
  }
  connect(dblsbRate, SIGNAL(valueChanged(double)), SLOT(setfFrequency(double)));
  layout->addRow(new QLabel("Rate: "), dblsbRate);

  // uWaveform
  cb = new QComboBox(layout->parentWidget());
  cb->addItem("Sine");
  cb->addItem("Ramp");
  cb->addItem("Pulse");
  cb->setCurrentIndex(uWaveform);
  connect(cb, SIGNAL(currentIndexChanged(int)), SLOT(setuWaveform(int)));
  layout->addRow(new QLabel("Waveform: "), cb);

  // bRelative
  QCheckBox * chb = new QCheckBox(layout->parentWidget());
  chb->setCheckState(bRelative ? Qt::Checked : Qt::Unchecked);
  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbRelative(int)));
  layout->addRow(new QLabel("Relative: "), chb);

  // bConstantRate
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bConstantRate ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbConstantRate(int)));
//  layout->addRow(new QLabel("bConstantrate: "), chb);
  // bCycleCount
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bCycleCount ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbCycleCount(int)));
//  layout->addRow(new QLabel("bCycleCount: "), chb);
  // bIncBlockCount
//  /*QCheckBox * */chb = new QCheckBox(layout->parentWidget());
//  chb->setCheckState(bIncBlockCount ? Qt::Checked : Qt::Unchecked);
//  connect(chb, SIGNAL(stateChanged(int)), SLOT(setbIncBlockCount(int)));
//  layout->addRow(new QLabel("bIncBlockCount: "), chb);
}

void MtlRampTask::run()
{
  Task::setMtlTaskState(Task::kBusy);
  // ����������� ���������� ������� �� ��������� �����
  MTL32::GDSSwitchControlMode(uCtrlCh, iMode);
  // �������� ������������ ������
  msleep(1000);
  // ��������� ���������� ������ �������
  MTL32::GDSPeakValleySegment(uCtrlCh,
                              (float) fToSetPoint,
                              (float) fGain,
                              (float) fFrequency,
                              (unsigned) uWaveform,
                              (BOOL) bRelative,
                              (BOOL) bConstantRate);
  // �������� ������������ ������
  msleep(1000);
  // ���������� ����� ���� ������ ��������� ������
  while (getMachineState() == kBusy)
  {
    msleep(1000);
  }
  Task::setMtlTaskState(Task::kIdle);
}


void MtlRampTask::write(QJsonObject & json)
{
  json["tasktype"] = QJsonValue("MTLRamp");

  json["Ctrl_Ch"] = QJsonValue(uCtrlCh);
  json["ControlMode"] = QJsonValue(iMode);
  json["SetPoint"] = QJsonValue(fToSetPoint);
  json["Rate"] = QJsonValue(fFrequency);
  json["Waveform"] = QJsonValue((int)uWaveform);
  json["Relative"] = QJsonValue((int)bRelative);
}

void MtlRampTask::read(const QJsonObject & json)
{
  uCtrlCh = json["Ctrl_Ch"].toInt();
  iMode = json["ControlMode"].toInt();
  fToSetPoint = (float)json["SetPoint"].toDouble();
  fFrequency = (float)json["Rate"].toDouble();
  bRelative = (unsigned)json["Relative"].toInt();
  uWaveform = (unsigned)json["Waveform"].toInt();
}

void MtlRampTask::setuCtrlCh(int val)
{
  uCtrlCh = val;
}

void MtlRampTask::setiMode(int val)
{
  iMode = val;
  if(val == 0 || val == 2)
  {
    dblsbSP->setSuffix(" mm");
    dblsbRate->setSuffix(" mm/s");
  }
  else if(val == 1)
  {
    dblsbSP->setSuffix(" kN");
    dblsbRate->setSuffix(" kN/s");
  }
}

void MtlRampTask::setfToSetPoint(double val)
{
  fToSetPoint = val;
}

void MtlRampTask::setfGain(double val)
{
  fGain = val;
}

void MtlRampTask::setfFrequency(double val)
{
  fFrequency = val;
}

void MtlRampTask::setuWaveform(int val)
{
  uWaveform = val;
}

void MtlRampTask::setbRelative(int val)
{
  bRelative = val;
}

void MtlRampTask::setbConstantRate(int val)
{
  bConstantRate = val;
}
